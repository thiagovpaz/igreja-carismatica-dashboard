import { Button } from '@chakra-ui/react';

interface PaginationItemProps {
  isCurrent?: boolean;
  number: number;
  onPageChange: (page: number) => void;
}

export default function PaginationItem({
  isCurrent = false,
  number,
  onPageChange,
}: PaginationItemProps) {
  if (isCurrent) {
    return (
      <Button
        size="sm"
        fontSize="xs"
        disabled
        color="white"
        _disabled={{
          cursor: 'default',
          bg: 'pink.500',
          opacity: 0.6,
        }}
        _hover={{
          bg: 'pink.400',
        }}
      >
        {number}
      </Button>
    );
  }

  return (
    <Button
      size="sm"
      fontSize="xs"
      colorScheme="pink"
      onClick={() => onPageChange(number)}
    >
      {number}
    </Button>
  );
}
