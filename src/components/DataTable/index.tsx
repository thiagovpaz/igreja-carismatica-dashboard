import { useState, useRef, useCallback } from 'react';
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  chakra,
  Spinner,
  Flex,
  Icon,
  Input,
  Checkbox,
  Button,
  Box,
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  useDisclosure,
} from '@chakra-ui/react';
import { TriangleDownIcon, TriangleUpIcon } from '@chakra-ui/icons';
import {
  ColumnDef,
  flexRender,
  getCoreRowModel,
  getSortedRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
  SortingState,
  FilterFn,
} from '@tanstack/react-table';
import { rankItem } from '@tanstack/match-sorter-utils';

import Pagination from './Pagination';

interface DataTableProps<T> {
  data: T[];
  columns: ColumnDef<T>[];
  isLoading: boolean;
  hasCheckbox?: boolean;
  deleteFn?: (ids: string[]) => Promise<void>;
  filterComponent?: JSX.Element;
}

export default function DataTable<T extends { id: string }>({
  data,
  columns,
  isLoading,
  hasCheckbox = true,
  deleteFn,
  filterComponent,
}: DataTableProps<T>) {
  const modalRef = useRef(null);

  const [sorting, setSorting] = useState<SortingState>([]);
  const [globalFilter, setGlobalFilter] = useState('');
  const [rowSelection, setRowSelection] = useState({});
  const [loading, setLoading] = useState(false);

  const { isOpen, onOpen, onClose } = useDisclosure();

  const fuzzyFilter: FilterFn<T> = (row, columnId, value, addMeta) => {
    const itemRank = rankItem(row.getValue(columnId), value);

    addMeta({
      itemRank,
    });

    return itemRank.passed;
  };

  let table_columns = columns;

  if (hasCheckbox) {
    table_columns = [
      {
        id: 'select',
        header: ({ table }: any) => (
          <Box p="1">
            <Checkbox
              {...{
                isChecked: table.getIsAllRowsSelected(),
                isIndeterminate: table.getIsSomeRowsSelected(),
                onChange: table.getToggleAllRowsSelectedHandler(),
              }}
            />
          </Box>
        ),
        cell: ({ row }: any) => (
          <Box p="1">
            <Checkbox
              {...{
                isChecked: row.getIsSelected(),
                isIndeterminate: row.getIsSomeSelected(),
                onChange: row.getToggleSelectedHandler(),
                value: row.original.id,
              }}
            />
            {JSON.stringify(row.getToggleSelectedHandler)}
          </Box>
        ),
      },
      ...columns,
    ];
  }

  const table = useReactTable({
    data,
    columns: table_columns,
    state: {
      sorting,
      globalFilter,
      rowSelection,
    },
    onGlobalFilterChange: setGlobalFilter,
    onRowSelectionChange: setRowSelection,
    globalFilterFn: fuzzyFilter,
    onSortingChange: setSorting,
    getCoreRowModel: getCoreRowModel(),
    getSortedRowModel: getSortedRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    debugTable: false,
  });

  const handleDeleteRows = useCallback(async () => {
    setLoading(true);
    const selectedRows = table
      .getFilteredSelectedRowModel()
      .rows.map((d) => d.original.id);

    if (deleteFn) {
      await deleteFn(selectedRows);
    }

    table.resetRowSelection();

    setLoading(false);

    onClose();
  }, [table, deleteFn, onClose]);

  return (
    <>
      <Flex py="4" justify="space-between">
        <Input
          flex={1}
          placeholder="Buscar"
          value={globalFilter ?? ''}
          onChange={(e) => setGlobalFilter(String(e.target.value))}
          w="254px"
        />

        <Flex flex={1} align="center" justify="center">
          {filterComponent}
        </Flex>

        <Flex flex={1} align="center" justify="center">
          {JSON.stringify(rowSelection) !== '{}' && (
            <Button colorScheme="red" onClick={onOpen}>
              Remover {Object.keys(rowSelection).length} selecionados
            </Button>
          )}
        </Flex>
      </Flex>
      <Table variant="simple" size="md">
        <Thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <Tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => {
                return (
                  <Th key={header.id} colSpan={header.colSpan}>
                    {header.isPlaceholder ? null : (
                      <Flex
                        cursor="pointer"
                        {...{
                          className: header.column.getCanSort()
                            ? 'cursor-pointer select-none'
                            : '',
                          onClick: header.column.getToggleSortingHandler(),
                        }}
                      >
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                        <chakra.span px="2">
                          {header.column.getIsSorted() ? (
                            header.column.getIsSorted() === 'asc' ? (
                              <Icon as={TriangleUpIcon} />
                            ) : (
                              <Icon as={TriangleDownIcon} />
                            )
                          ) : null}
                        </chakra.span>
                      </Flex>
                    )}
                  </Th>
                );
              })}
            </Tr>
          ))}
        </Thead>
        <Tbody>
          {table.getRowModel().rows.map((row) => (
            <Tr key={row.id}>
              {row.getVisibleCells().map((cell) => (
                <Td key={cell.id}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </Td>
              ))}
            </Tr>
          ))}
          {isLoading && (
            <Tr>
              <Td colSpan={columns.length + 1} textAlign="center">
                <Spinner size="sm" />
              </Td>
            </Tr>
          )}
          {table.getRowModel().rows.length === 0 && !isLoading && (
            <Tr>
              <Td colSpan={columns.length + 1} textAlign="center">
                NENHUM DADO ENCONTRADO
              </Td>
            </Tr>
          )}
        </Tbody>
        <Tfoot>
          {table.getHeaderGroups().map((headerGroup) => (
            <Tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => {
                return (
                  <Th key={header.id} colSpan={header.colSpan}>
                    {header.isPlaceholder ? null : (
                      <Flex
                        cursor="pointer"
                        {...{
                          className: header.column.getCanSort()
                            ? 'cursor-pointer select-none'
                            : '',
                          onClick: header.column.getToggleSortingHandler(),
                        }}
                      >
                        {flexRender(
                          header.column.columnDef.header,
                          header.getContext(),
                        )}
                        <chakra.span px="2">
                          {header.column.getIsSorted() ? (
                            header.column.getIsSorted() === 'asc' ? (
                              <Icon as={TriangleUpIcon} />
                            ) : (
                              <Icon as={TriangleDownIcon} />
                            )
                          ) : null}
                        </chakra.span>
                      </Flex>
                    )}
                  </Th>
                );
              })}
            </Tr>
          ))}
        </Tfoot>
      </Table>

      <Pagination
        totalCountOfRegisters={table.getPrePaginationRowModel().rows.length}
        registersPerPage={10}
        currentPage={table.getState().pagination.pageIndex + 1}
        onPageChange={(page) => table.setPageIndex(page - 1)}
      />

      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        leastDestructiveRef={modalRef}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Remover items
            </AlertDialogHeader>

            <AlertDialogBody>
              Você tem certeza que deseja remover ?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button onClick={onClose} color="gray.900 ">
                Cancelar
              </Button>
              <Button
                colorScheme="red"
                isLoading={loading}
                onClick={handleDeleteRows}
                ml={3}
              >
                Remover
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </>
  );
}
