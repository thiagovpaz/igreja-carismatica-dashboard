import {
  forwardRef,
  ForwardRefRenderFunction,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Avatar,
  Box,
  Button,
  Flex,
  Icon,
  Slider,
  SliderFilledTrack,
  SliderThumb,
  SliderTrack,
  InputProps,
  Input,
} from '@chakra-ui/react';
import { FaCamera, FaTrash } from 'react-icons/fa';
import { useFormContext } from 'react-hook-form';
import AvatarEditor from 'react-avatar-editor';

import api from '../../services/api';

type AvatarUploadProps = InputProps;

const AvatarUploadBase: ForwardRefRenderFunction<
  HTMLInputElement,
  AvatarUploadProps
> = ({ ...rest }, ref) => {
  const editorRef = useRef<AvatarEditor>(null);
  const inputRef = useRef<HTMLInputElement>(null);
  const dialogRef = useRef(null);

  const { setValue, watch } = useFormContext();

  const [showDeleteDialog, setShowDeleteDialog] = useState(false);
  const [showCropDialog, setShowCropDialog] = useState(false);
  const [scale, setScale] = useState(1);
  const [preview, setPreview] = useState<string | null>(null);
  const [loadedFile, setLoadedFile] = useState<string | null>(null);

  const toggleRemoveAvatarDialog = () => {
    setShowDeleteDialog((oldState) => !oldState);
  };

  const toggleCropAvatarDialog = () => {
    setShowCropDialog((oldState) => !oldState);
  };

  const handleDeleteAvatar = () => {
    setPreview(null);
    setLoadedFile(null);
    setValue('avatar_file', '');
    toggleRemoveAvatarDialog();
  };

  const onSelectFile = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      if (e.target.files && e.target.files[0]) {
        setPreview(URL.createObjectURL(e.target.files[0]));
        toggleCropAvatarDialog();
      }
      return () => URL.revokeObjectURL(preview!);
    },
    [preview],
  );

  const handleUpload = useCallback(
    async (data: Blob) => {
      const formData = new FormData();

      formData.append('file', data, 'avatar.png');

      const response = await api.post('/files', formData);

      setValue('avatar_file', response.data.path);

      setLoadedFile(null);
    },
    [setValue],
  );

  const getResultImage = useCallback(async () => {
    const dataUrl = editorRef?.current?.getImageScaledToCanvas().toDataURL();
    const result = await fetch(dataUrl!);
    const blob = await result.blob();
    const b = blob && URL.createObjectURL(blob);

    setPreview(b);
    toggleCropAvatarDialog();
    handleUpload(blob);

    return () => URL.revokeObjectURL(b);
  }, [handleUpload]);

  const avatar = watch('avatar_file');

  useEffect(() => {
    if (avatar) {
      setLoadedFile(`${import.meta.env.VITE_API_URI}/avatars/${avatar}`);
    }
  }, [avatar]);

  return (
    <Flex justify="center" align="center">
      <Input ref={ref} {...rest} display="none" />
      <Avatar src={loadedFile || preview!} size="2xl" position="relative">
        <input
          ref={inputRef}
          type="file"
          style={{ display: 'none' }}
          accept="image/*"
          onChange={onSelectFile}
        />
        <Button
          variant="solid"
          colorScheme="red"
          size="xs"
          position="absolute"
          bottom={0}
          left={0}
          onClick={toggleRemoveAvatarDialog}
        >
          <Icon as={FaTrash} color="white" />
        </Button>
        <Button
          variant="solid"
          colorScheme="blue"
          size="xs"
          position="absolute"
          bottom={0}
          right={0}
          onClick={() => inputRef.current?.click()}
        >
          <Icon as={FaCamera} color="white" />
        </Button>
      </Avatar>

      <AlertDialog
        isOpen={showCropDialog}
        onClose={toggleCropAvatarDialog}
        leastDestructiveRef={dialogRef}
        closeOnOverlayClick={false}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Cortar
            </AlertDialogHeader>
            <AlertDialogBody
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <AvatarEditor
                crossOrigin="anonymous"
                ref={editorRef}
                image={preview!}
                width={147}
                height={147}
                border={50}
                color={[0, 0, 0, 0.6]}
                scale={scale}
                rotate={0}
                borderRadius={72.3}
              />
            </AlertDialogBody>
            <AlertDialogFooter>
              <Flex w="full" direction="column">
                <Flex w="100%" pb="6">
                  <Box pr="20px">Tamanho</Box>
                  <Slider
                    aria-label="avatar-scale"
                    defaultValue={1}
                    step={0.1}
                    max={5}
                    onChange={(s) => setScale(s)}
                  >
                    <SliderTrack>
                      <SliderFilledTrack />
                    </SliderTrack>
                    <SliderThumb />
                  </Slider>
                </Flex>
                <Flex w="100%" justify="center">
                  <Button colorScheme="blue" onClick={getResultImage}>
                    Cortar
                  </Button>
                  {/* <Button
                    ml={3}
                    colorScheme="red"
                    onClick={toggleCropAvatarDialog}
                  >
                    Fechar
                  </Button> */}
                </Flex>
              </Flex>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>

      <AlertDialog
        leastDestructiveRef={dialogRef}
        isOpen={showDeleteDialog}
        onClose={toggleRemoveAvatarDialog}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Remover Avatar
            </AlertDialogHeader>

            <AlertDialogBody>
              Você tem certeza que deseja remover ?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button colorScheme="blue" onClick={toggleRemoveAvatarDialog}>
                Fechar
              </Button>
              <Button colorScheme="red" ml={3} onClick={handleDeleteAvatar}>
                Remover
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Flex>
  );
};

const AvatarUpload = forwardRef(AvatarUploadBase);

export { AvatarUpload };
