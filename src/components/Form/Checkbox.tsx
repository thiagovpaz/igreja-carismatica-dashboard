import { forwardRef, ForwardRefRenderFunction } from 'react';
import {
  FormControl,
  FormErrorMessage,
  Checkbox as ChackraCheckbox,
  CheckboxProps as ChackraCheckboxProps,
} from '@chakra-ui/react';
import { Controller, FieldError } from 'react-hook-form';

interface CheckboxProps extends ChackraCheckboxProps {
  name: string;
  error?: FieldError;
  children: React.ReactNode;
}

const CheckboxBase: ForwardRefRenderFunction<
  HTMLInputElement,
  CheckboxProps
> = ({ children, name, error = null, ...rest }, ref) => {
  return (
    <FormControl isInvalid={!!error}>
      <Controller
        name={name}
        render={({ field }) => {
          return (
            <ChackraCheckbox
              {...rest}
              isChecked={!!field.value}
              onChange={field.onChange}
              value={String(field.value)}
            >
              {children}
            </ChackraCheckbox>
          );
        }}
      />
      {!!error && <FormErrorMessage>{error.message}</FormErrorMessage>}
    </FormControl>
  );
};

const Checkbox = forwardRef(CheckboxBase);

export { Checkbox };
