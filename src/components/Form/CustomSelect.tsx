import { FormControl, FormLabel, FormErrorMessage } from '@chakra-ui/react';
import {
  FieldError,
  Controller,
  DeepRequired,
  FieldErrorsImpl,
  Merge,
} from 'react-hook-form';
import { ChakraStylesConfig, Select, Props } from 'chakra-react-select';

const chakraStyles: ChakraStylesConfig = {
  container: (provided) => ({
    ...provided,
  }),
  menuList: (provided) => ({
    ...provided,
    background: 'gray.800',
  }),
  inputContainer: (provided) => ({
    ...provided,
  }),
  input: (provided) => ({
    ...provided,
    width: '250px',
  }),
  placeholder: (provided) => ({
    ...provided,
  }),
  option: (provided, state) => ({
    ...provided,
    color: state.isFocused ? 'gray.800' : 'white',
  }),
  dropdownIndicator: (provided) => ({
    ...provided,
    background: 'gray.800',
  }),
};

type OptionItem = {
  id: string | null;
  name: string;
};

interface SelectProps extends Props {
  label: string;
  name: string;
  options: OptionItem[];
  error?:
    | FieldError
    | Merge<
        FieldError,
        FieldErrorsImpl<
          DeepRequired<{
            value: string;
          }>
        >
      >;
}

const CustomSelect: React.FC<SelectProps> = ({
  label,
  name,
  error = null,
  options,
  ...rest
}) => {
  return (
    <FormControl isInvalid={!!error}>
      <FormLabel>{label}</FormLabel>
      <Controller
        name={name}
        render={({ field }) => {
          return (
            <Select
              colorScheme="red"
              chakraStyles={chakraStyles}
              onChange={field.onChange}
              options={options.map((o) => ({ label: o.name, value: o.id }))}
              value={field.value}
              {...rest}
            />
          );
        }}
      />
      {!!error && <FormErrorMessage>{error.message}</FormErrorMessage>}
    </FormControl>
  );
};

export { CustomSelect };
