/* eslint-disable react/no-this-in-sfc */
import { forwardRef, ForwardRefRenderFunction } from 'react';
import ReactQuill from 'react-quill';
import { Controller, FieldError } from 'react-hook-form';
import { FormControl, FormErrorMessage, FormLabel } from '@chakra-ui/react';

import EditorToolbar, { formats, modules } from './EditorToolbar';

import 'react-quill/dist/quill.snow.css';

interface EditorProps {
  label: string;
  name: string;
  error?: FieldError;
}

const EditorBase: ForwardRefRenderFunction<ReactQuill, EditorProps> = (
  { label, name, error = null },
  ref,
) => {
  return (
    <FormControl isInvalid={!!error}>
      {!!label && <FormLabel htmlFor={label}>{label}</FormLabel>}
      <Controller
        name={name}
        render={({ field: { onChange, onBlur, value } }) => {
          return (
            <>
              <EditorToolbar />
              <ReactQuill
                value={value}
                defaultValue={value}
                onChange={onChange}
                onBlur={onBlur}
                theme="snow"
                modules={modules}
                formats={formats}
                style={{ height: '350px' }}
                ref={ref}
              />
            </>
          );
        }}
      />

      {!!error && <FormErrorMessage>{error.message}</FormErrorMessage>}
    </FormControl>
  );
};

const Editor = forwardRef(EditorBase);

export { Editor };
