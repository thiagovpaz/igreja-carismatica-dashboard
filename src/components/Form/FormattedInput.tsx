import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
} from '@chakra-ui/react';
import { forwardRef, ForwardRefRenderFunction } from 'react';
import { Controller, FieldError } from 'react-hook-form';
import NumberFormat from 'react-number-format';

interface FormattedInputProps {
  label: string;
  name: string;
  placeholder: string;
  error?: FieldError;
  control: any;
}

const FormattedInputBase: ForwardRefRenderFunction<
  HTMLInputElement,
  FormattedInputProps
> = ({ label, name, control, error = null, ...rest }, ref) => {
  return (
    <FormControl isInvalid={!!error}>
      {!!label && <FormLabel htmlFor={name}>{label}</FormLabel>}
      <Controller
        name={name}
        control={control}
        render={({ field }) => {
          return (
            <NumberFormat
              focusBorderColor="blue.500"
              bg="gray.900"
              variant="filled"
              _hover={{
                bg: 'gray.900',
              }}
              size="lg"
              customInput={Input}
              thousandSeparator="."
              decimalSeparator=","
              decimalScale={2}
              fixedDecimalScale
              allowEmptyFormatting
              {...field}
              {...rest}
            />
          );
        }}
      />

      {!!error && <FormErrorMessage>{error.message}</FormErrorMessage>}
    </FormControl>
  );
};

const FormattedInput = forwardRef(FormattedInputBase);

export default FormattedInput;
