import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Select as ChackraSelect,
  SelectProps as ChackraSelectProps,
} from '@chakra-ui/react';
import { forwardRef, ForwardRefRenderFunction } from 'react';
import { FieldError } from 'react-hook-form';

type OptionItem = {
  id: string | null;
  name: string;
};

interface SelectProps extends ChackraSelectProps {
  label: string;
  name: string;
  options: OptionItem[];
  error?: FieldError;
}

const SelectBase: ForwardRefRenderFunction<HTMLSelectElement, SelectProps> = (
  { label, name, error = null, options, ...rest },
  ref,
) => {
  return (
    <FormControl isInvalid={!!error}>
      <FormLabel>{label}</FormLabel>
      <ChackraSelect
        w="100%"
        ref={ref}
        id={name}
        name={name}
        {...rest}
        focusBorderColor="blue.500"
        bg="gray.900"
        variant="filled"
        size="lg"
        _hover={
          {
            // background: 'red',
          }
        }
      >
        {options.map((o) => {
          return (
            <option key={String(o.id)} value={String(o.id)}>
              {o.name}
            </option>
          );
        })}
      </ChackraSelect>
      {!!error && <FormErrorMessage>{error.message}</FormErrorMessage>}
    </FormControl>
  );
};

const Select = forwardRef(SelectBase);

export { Select };
