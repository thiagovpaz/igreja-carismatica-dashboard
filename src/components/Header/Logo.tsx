import { Text } from '@chakra-ui/react';

export default function Logo() {
  return (
    <Text
      fontSize={['md', 'xl']}
      fontWeight="bold"
      letterSpacing="tight"
      w={64}
    >
      {import.meta.env.VITE_APP_NAME}
    </Text>
  );
}
