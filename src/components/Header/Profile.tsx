import { Flex, Box, Avatar, Text } from '@chakra-ui/react';

import { useAuth } from '../../hooks/auth';

interface ProfileProps {
  showProfileUserData: boolean;
}

export default function Profile({ showProfileUserData }: ProfileProps) {
  const { user } = useAuth();

  return (
    <Flex align="center">
      {showProfileUserData && (
        <Box mr="4" textAlign="right">
          <Text>{user.first_name}</Text>

          <Text color="gray.300" fontSize="small">
            {user.email}
          </Text>
        </Box>
      )}

      <Avatar size="md" name={`${user.first_name} ${user.last_name}`} />
    </Flex>
  );
}
