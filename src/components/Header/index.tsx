import { Flex, Icon, IconButton, useBreakpointValue } from '@chakra-ui/react';
import { RiMenuLine } from 'react-icons/ri';

import { useSidebarDrawer } from '../../hooks/sidebar';

import Logo from './Logo';
import Profile from './Profile';
import SearchBox from './SearchBox';

export default function Header() {
  const isWideScreen = useBreakpointValue({ base: false, lg: true }) as boolean;
  const { onOpen } = useSidebarDrawer();

  return (
    <Flex
      as="header"
      w="100%"
      maxWidth={1480}
      h="20"
      mx="auto"
      mt="4"
      px="6"
      align="center"
    >
      {!isWideScreen && (
        <IconButton
          aria-label="Open Navigation"
          variant="unstyled"
          icon={<Icon as={RiMenuLine} />}
          onClick={onOpen}
          fontSize="24"
          mr="2"
          mt="7px"
        />
      )}

      <Logo />

      {/* {isWideScreen && <SearchBox />} */}

      <Flex align="center" ml="auto">
        <Profile showProfileUserData={isWideScreen} />
      </Flex>
    </Flex>
  );
}
