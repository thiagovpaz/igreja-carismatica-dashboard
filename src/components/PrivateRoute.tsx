import { Navigate, useLocation } from 'react-router-dom';

import { useAuth } from '../hooks/auth';

interface PrivateRouteProps {
  checkAuth?: boolean;
  children: React.ReactNode;
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({
  checkAuth = false,
  children,
}) => {
  const { user } = useAuth();

  const location = useLocation();

  if ((checkAuth && user) || (location.pathname === '/' && user)) {
    return <Navigate to="/dashboard" state={{ from: location }} replace />;
  }

  if (!checkAuth && !user) {
    return <Navigate to="/signin" state={{ from: location }} replace />;
  }

  return <div>{children}</div>;
};

export default PrivateRoute;
