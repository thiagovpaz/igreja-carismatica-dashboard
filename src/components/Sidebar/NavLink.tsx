import { ElementType } from 'react';
import {
  Icon,
  Text,
  Flex,
  Link as ChackraLink,
  LinkProps as ChackraLinkProps,
} from '@chakra-ui/react';
import {
  NavLink as NavLinkRRD,
  useMatch,
  useResolvedPath,
} from 'react-router-dom';

interface NavLinkPros extends ChackraLinkProps {
  icon: ElementType;
  children: string;
  to: string;
}

export default function NavLink({ icon, children, to, ...rest }: NavLinkPros) {
  const resolved = useResolvedPath(to);
  const match = useMatch({ path: resolved.pathname, end: false });

  return (
    <ChackraLink to={to} as={NavLinkRRD} color={match ? 'pink.400' : 'gray.50'}>
      <Flex display="flex" alignItems="center">
        <Icon as={icon} fontSize="20" />
        <Text ml="4" fontWeight="medium">
          {children}
        </Text>
      </Flex>
    </ChackraLink>
  );
}
