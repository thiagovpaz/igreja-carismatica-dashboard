import { Stack, Button } from '@chakra-ui/react';
import {
  RiDashboardLine,
  RiContactsLine,
  RiNotification2Line,
  RiMoneyDollarBoxLine,
  RiMessageLine,
  RiPagesFill,
} from 'react-icons/ri';
import { FaCogs } from 'react-icons/fa';

import { useAuth } from '../../hooks/auth';

import NavLink from './NavLink';
import NavSection from './NavSection';

export default function SidebarNav() {
  const { signOut } = useAuth();

  return (
    <Stack spacing="12" align="flex-start">
      <NavSection title="Geral">
        <NavLink to="/dashboard" icon={RiDashboardLine}>
          Dashboard
        </NavLink>

        <NavLink to="/users" icon={RiContactsLine}>
          Usuários
        </NavLink>

        <NavLink to="/testimonies" icon={RiMessageLine}>
          Testemunhos
        </NavLink>

        <NavLink to="/donations" icon={RiMoneyDollarBoxLine}>
          Doações / Dízimos
        </NavLink>

        <NavLink to="/notifications" icon={RiNotification2Line}>
          Notificações
        </NavLink>

        <NavLink to="/pages" icon={RiPagesFill}>
          Páginas
        </NavLink>
      </NavSection>

      <NavSection title="Configurações">
        <NavLink to="/settings" icon={FaCogs}>
          Configurações
        </NavLink>
      </NavSection>

      <NavSection title="">
        <Button
          onClick={signOut}
          variant="unstyled"
          _hover={{
            textDecoration: 'underline',
          }}
        >
          Sair
        </Button>
      </NavSection>
    </Stack>
  );
}
