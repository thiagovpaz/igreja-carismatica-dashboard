import React, {
  createContext,
  useCallback,
  useState,
  useContext,
  useMemo,
} from 'react';

import api from '../services/api';

interface User {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  user: User;
  signIn: (credentials: SignInCredentials) => Promise<void>;
  signOut: () => void;
  isLoading: boolean;
}

interface AuthState {
  token: string;
  user: User;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

interface AuthProviderProps {
  children: React.ReactNode;
}

const AuthProvider: React.FC<AuthProviderProps> = ({ children }) => {
  const [loading, setLoading] = useState(false);

  const [data, setData] = useState<AuthState>(() => {
    const token = localStorage.getItem('[igreja_carismatica]:token');
    const user = localStorage.getItem('[igreja_carismatica]:user');

    if (token && user) {
      api.defaults.headers.common.Authorization = `Bearer ${token}`;
      return { token, user: JSON.parse(user) as User };
    }

    return {} as AuthState;
  });

  const signIn = useCallback(async ({ email, password }: SignInCredentials) => {
    setLoading(true);

    try {
      const response = await api.post('/sessions', { email, password });

      const { token, user } = response.data;

      api.defaults.headers.common.Authorization = `Bearer ${token}`;

      localStorage.setItem('[igreja_carismatica]:token', token);
      localStorage.setItem('[igreja_carismatica]:user', JSON.stringify(user));

      setData({ token, user });
    } catch (error: any) {
      throw new Error(error);
    } finally {
      setLoading(false);
    }
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('[igreja_carismatica]:token');
    localStorage.removeItem('[igreja_carismatica]:user');

    setData({} as AuthState);
  }, []);

  const context = useMemo<AuthContextData>(
    () => ({
      user: data.user,
      signIn,
      signOut,
      isLoading: loading,
    }),
    [data.user, loading, signIn, signOut],
  );

  return (
    <AuthContext.Provider value={context}>{children}</AuthContext.Provider>
  );
};

const useAuth = (): AuthContextData => {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('Must be inside a provider.');
  }

  return context;
};

export { AuthProvider, useAuth };
