import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.min.css';

import { AuthProvider } from './auth';

import { theme } from '../styles/theme';
import { SidebarDrawerProvider } from './sidebar';

interface AppProviderProps {
  children: React.ReactNode;
}

export const AppProvider: React.FC<AppProviderProps> = ({ children }) => (
  <BrowserRouter>
    <ChakraProvider theme={theme}>
      <SidebarDrawerProvider>
        <AuthProvider>
          {children}
          <ToastContainer />
        </AuthProvider>
      </SidebarDrawerProvider>
    </ChakraProvider>
  </BrowserRouter>
);
