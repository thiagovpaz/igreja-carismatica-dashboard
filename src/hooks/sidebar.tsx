import React, { useContext, createContext, useEffect } from 'react';
import { useDisclosure, UseDisclosureReturn } from '@chakra-ui/react';
import { useLocation } from 'react-router-dom';

type SidebarDrawerContextData = UseDisclosureReturn;

const SidebarDrawerContext = createContext({} as SidebarDrawerContextData);

interface SidebarDrawerProviderProps {
  children: React.ReactNode;
}

export const SidebarDrawerProvider: React.FC<SidebarDrawerProviderProps> = ({
  children,
}) => {
  const disclosure = useDisclosure();
  const location = useLocation();

  useEffect(() => {
    disclosure.onClose();
  }, [location]);

  return (
    <SidebarDrawerContext.Provider value={disclosure}>
      {children}
    </SidebarDrawerContext.Provider>
  );
};

export const useSidebarDrawer = () => useContext(SidebarDrawerContext);
