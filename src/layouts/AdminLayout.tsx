import { Flex } from '@chakra-ui/react';
import { Outlet } from 'react-router-dom';

import Header from '../components/Header';
import Sidebar from '../components/Sidebar';

export function AdminLayout() {
  return (
    <Flex direction="column" minH="100vh">
      <Header />
      <Flex w="100%" minH="100%" maxWidth={1480} my="6" mx="auto" px="6">
        <Sidebar />
        <Outlet />
      </Flex>
    </Flex>
  );
}
