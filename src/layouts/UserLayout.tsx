import { Flex } from '@chakra-ui/react';
import { Outlet } from 'react-router-dom';

export function UserLayout() {
  return (
    <Flex direction="column" minH="100vh" justify="center" align="center">
      <Outlet />
    </Flex>
  );
}
