import { useCallback, useEffect } from 'react';
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { useForm, FormProvider } from 'react-hook-form';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { TextInput } from '../../components/Form/TextInput';
import { TextArea } from '../../components/Form/TextArea';
import { Select } from '../../components/Form/Select';
import { Editor } from '../../components/Form/Editor';

import api from '../../services/api';

interface PageFormData {
  title: string;
  subtitle: string;
  content: string;
}

const schema = yup.object({
  title: yup.string().required('Campo Obrigatório'),
  content: yup.string().required('Campo Obrigatório'),
});

const Edit = () => {
  const navigate = useNavigate();

  const { id } = useParams();

  const form = useForm<PageFormData>({
    defaultValues: {
      // content: id,
    },
    resolver: yupResolver(schema),
  });

  const {
    setValue,
    reset,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(
    async (data: PageFormData) => {
      await toast.promise(
        api.put(`/pages/${id}`, data),
        {
          pending: 'Salvando página',
          success: 'Página salva com sucesso',
          error: 'Falha ao salvar página',
        },
        {
          autoClose: 2000,
          onClose: () => {
            navigate('/pages');
          },
        },
      );
    },
    [navigate, id],
  );

  useEffect(() => {
    api.get(`/pages/${id}`).then((response) => {
      reset(response.data);
    });
  }, [reset, id, setValue]);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex
        as="form"
        w="100%"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Heading as="h1" size="lg">
          Editar página
        </Heading>
        <Divider my="6" borderColor="gray.700" />
        <FormProvider {...form}>
          <VStack width="100%" spacing="8">
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Título"
                placeholder="Título"
                error={errors.title}
                {...register('title')}
              />
              <TextInput
                label="Sub-Título"
                placeholder="Sub-Título"
                error={errors.subtitle}
                {...register('subtitle')}
              />
            </SimpleGrid>

            <Editor
              label="Conteúdo"
              error={errors.content}
              {...register('content')}
            />
          </VStack>
        </FormProvider>

        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button as={NavLink} to="/pages" colorScheme="whiteAlpha">
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Edit;
