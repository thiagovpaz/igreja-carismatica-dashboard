import { useState, useEffect } from 'react';
import {
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import api from '../../services/api';

import DataTable from '../../components/DataTable';

interface Page {
  id: string;
  title: string;
  subtitle: string;
  content: string;
  created_at: Date;
}

interface ActionsButtonsProps {
  id: string;
}

const Content = () => {
  const [pages, setPages] = useState<Page[]>([]);
  const [loading, setLoading] = useState(true);
  const [preview, setPreview] = useState<Page | undefined>();

  useEffect(() => {
    async function loadPages() {
      const response = await api.get('/pages');

      if (response.data) {
        setPages(response.data);
        setLoading(false);
      }
    }
    loadPages();
  }, []);

  const ActionsButtons: React.FC<ActionsButtonsProps> = ({ id }) => {
    const navigate = useNavigate();

    const handleView = () => {
      setPreview(pages.find((n) => n.id === id));
    };

    const handleEdit = () => {
      navigate(`/pages/edit/${id}`);
    };

    return (
      <Flex gap="2" w="100%" justifyContent="center">
        <Button
          onClick={handleView}
          size="sm"
          colorScheme="yellow"
          variant="solid"
        >
          Ver
        </Button>
        <Button
          onClick={handleEdit}
          size="sm"
          colorScheme="blue"
          variant="solid"
        >
          Editar
        </Button>
        <CopyToClipboard
          text={id}
          onCopy={() => {
            toast(`ID Copiado!`, {
              type: 'success',
              position: 'top-right',
            });
          }}
        >
          <Button size="sm" colorScheme="red" variant="solid">
            ID
          </Button>
        </CopyToClipboard>
      </Flex>
    );
  };

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex w="100%" justify="space-between" align="flex-start">
        <Heading as="h1" size="lg">
          Páginas ({pages.length})
        </Heading>
      </Flex>
      <Divider my="6" borderColor="gray.700" />

      <DataTable<Page>
        data={pages}
        columns={[
          {
            accessorKey: 'title',
            cell: (info) => info.getValue(),
            header: () => 'Título',
            footer: () => 'Título',
          },
          {
            accessorKey: 'id',
            cell: (info) => <ActionsButtons id={info.getValue()} />,
            header: () => 'Ações',
            footer: () => 'Ações',
          },
        ]}
        isLoading={loading}
        hasCheckbox={false}
      />

      <Modal isOpen={!!preview} onClose={() => setPreview(undefined)}>
        <ModalOverlay />
        <ModalContent bg="gray.800">
          <ModalHeader>{preview?.title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text>{preview?.subtitle}</Text>
            {preview?.content && (
              <Box
                p={4}
                dangerouslySetInnerHTML={{ __html: preview.content }}
              />
            )}
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme="yellow"
              mr={3}
              onClick={() => setPreview(undefined)}
            >
              Fechar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

export default Content;
