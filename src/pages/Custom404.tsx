import { Flex, Stack, Heading, Text, Image, Button } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

import noDataImage from '../assets/images/404.svg';

function Custom404() {
  const navigate = useNavigate();

  const handleHomePage = () => {
    navigate('/');
  };

  return (
    <Flex
      w="100%"
      maxWidth={1440}
      mx="auto"
      h="100vh"
      justify="center"
      align="center"
      bg="gray.100"
      px="20px"
    >
      <Stack
        w={360}
        p="8"
        borderRadius="6"
        boxShadow="lg"
        spacing="4"
        align="center"
        justify="space-between"
        bg="white"
      >
        <Heading size="lg" color="gray.500">
          404
        </Heading>
        <Text size="lg" color="gray.500">
          Página não encontrada
        </Text>
        <Image src={noDataImage} alt="No data" w={160} />
        <Button
          variant="solid"
          size="md"
          colorScheme="blue"
          borderRadius="50"
          px="8"
          onClick={handleHomePage}
        >
          Início
        </Button>
      </Stack>
    </Flex>
  );
}

export default Custom404;
