import { useEffect, useState } from 'react';
import {
  Box,
  Divider,
  Flex,
  Heading,
  SimpleGrid,
  Text,
  theme,
} from '@chakra-ui/react';
import { startOfWeek, endOfWeek } from 'date-fns';
import Chart, { Props as ChartProps } from 'react-apexcharts';

import api from '../services/api';

const localeConfig = [
  {
    name: 'en',
    options: {
      months: [
        'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro',
      ],
      shortMonths: [
        'Jan',
        'Fev',
        'Mar',
        'Abr',
        'Mai',
        'Jun',
        'Jul',
        'Ago',
        'Set',
        'Out',
        'Nov',
        'Dez',
      ],
      days: [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
      ],
      shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      toolbar: {
        exportToSVG: 'Download SVG',
        exportToPNG: 'Download PNG',
        menu: 'Menu',
        selection: 'Selection',
        selectionZoom: 'Selection Zoom',
        zoomIn: 'Zoom In',
        zoomOut: 'Zoom Out',
        pan: 'Panning',
        reset: 'Reset Zoom',
      },
    },
  },
];

interface AxisType {
  date: Date;
  count: number;
}

const Dashboard = () => {
  const [users, setUsers] = useState([]);
  const [donations, setDonations] = useState([]);

  const options_donations: ChartProps = {
    chart: {
      locales: localeConfig,
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
      foreColor: theme.colors.gray[500],
    },
    grid: {
      show: false,
    },
    dataLabels: {
      enabled: true,
    },
    tooltip: {
      enabled: false,
    },
    xaxis: {
      type: 'datetime',
      axisBorder: {
        color: theme.colors.gray[600],
      },
      axisTicks: {
        color: theme.colors.gray[600],
      },
      categories: donations.map((u: AxisType) => u.date),
      min: startOfWeek(new Date()).getTime(),
      max: endOfWeek(new Date()).getTime(),
    },
    fill: {
      opacity: 0.3,
      type: 'gradient',
      gradient: {
        shade: 'dark',
        opacityFrom: 0.7,
        opacityTo: 0.3,
      },
    },
  };

  const options_users: ChartProps = {
    chart: {
      locales: localeConfig,
      toolbar: {
        show: false,
      },
      zoom: {
        enabled: false,
      },
      foreColor: theme.colors.gray[500],
    },
    grid: {
      show: false,
    },
    dataLabels: {
      enabled: true,
    },
    tooltip: {
      enabled: false,
    },
    xaxis: {
      type: 'datetime',
      axisBorder: {
        color: theme.colors.gray[600],
      },
      axisTicks: {
        color: theme.colors.gray[600],
      },
      categories: users.map((u: AxisType) => u.date),
      min: startOfWeek(new Date()).getTime(),
      max: endOfWeek(new Date()).getTime(),
    },
    fill: {
      opacity: 0.3,
      type: 'gradient',
      gradient: {
        shade: 'dark',
        opacityFrom: 0.7,
        opacityTo: 0.3,
      },
    },
  };

  const series_users = [
    {
      name: 'series_users',
      data: users.map((u: AxisType) => u.count),
    },
  ];

  const series_donations = [
    {
      name: 'series_donations',
      data: donations.map((u: AxisType) => u.count),
    },
  ];

  useEffect(() => {
    async function getGraphs() {
      const response_users = await api.get('/graphs/users');
      if (response_users.data) {
        setUsers(response_users.data);
        console.log(startOfWeek(new Date()));
      }

      const response_donations = await api.get('/graphs/donations');

      if (response_donations.data) {
        setDonations(response_donations.data);
      }
    }

    getGraphs();
  }, []);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Heading as="h1" size="lg">
        Dashboard
      </Heading>
      <Divider my="6" borderColor="gray.700" />
      <Flex w="100%" h="100%" justify="flex-start" align="flex-start">
        <SimpleGrid
          flex="1"
          gap="4"
          minChildWidth="320px"
          alignItems="flex-tart"
        >
          <Box p={['2', '6']} bg="gray.700" borderRadius={8}>
            <Text fontSize="lg" mb="4">
              Cadastros da Semana
            </Text>

            <Chart
              options={options_users}
              series={series_users}
              type="area"
              height={160}
            />
          </Box>
          <Box p={['2', '6']} bg="gray.700" borderRadius={8}>
            <Text fontSize="lg" mb="4">
              Doações / Dízimos
            </Text>

            <Chart
              options={options_donations}
              series={series_donations}
              type="area"
              height={160}
            />
          </Box>
        </SimpleGrid>
      </Flex>
    </Flex>
  );
};

export default Dashboard;
