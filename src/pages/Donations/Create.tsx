import { useCallback, useEffect, useState } from 'react';
import {
  Button,
  Checkbox,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
  Text,
} from '@chakra-ui/react';
import { useForm, FormProvider } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { NavLink, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import api from '../../services/api';

import FormattedInput from '../../components/Form/FormattedInput';
import { Select } from '../../components/Form/Select';
import { CustomSelect } from '../../components/Form/CustomSelect';
import { TextInput } from '../../components/Form/TextInput';
import { TextArea } from '../../components/Form/TextArea';

interface User {
  id: string;
  full_name: string;
}

interface DonationFormData {
  user?: User;
  user_name?: string;
  user_address?: string;
  value: number;
  user_id?: { value: string };
  payment_method: string;
  type: string;
  status: string;
}

interface Option {
  id: string;
  name: string;
}

const donation_types = [
  { id: 'donation', name: 'Doação' },
  { id: 'tithe', name: 'Dízimo' },
  { id: 'candle', name: 'Vela' },
  { id: 'cord', name: 'Cordão' },
];

const payment_types = [
  { id: 'credit_card', name: 'Cartão de crédito' },
  { id: 'pix', name: 'Pix' },
];

const status_types = [
  { id: 'created', name: 'Iniciado' },
  { id: 'finished', name: 'Finalizado' },
];

const Create = () => {
  const navigate = useNavigate();

  const [users, setUsers] = useState<Option[]>([]);
  const [has_user, setHasUser] = useState(true);

  const schema = yup.object({
    user_id: has_user ? yup.mixed().required('Campo Obrigatório') : yup.mixed(),
    user_name: !has_user
      ? yup.string().required('Campo Obrigatório')
      : yup.string(),
    payment_method: yup.string().required('Campo Obrigatório'),
    type: yup.string().required('Campo Obrigatório'),
    value: yup
      .string()
      .not(['0,00', '0'], 'Por favor informe um valor acima de zero')
      .required('Campo Obrigatório'),
  });

  const form = useForm<DonationFormData>({
    defaultValues: {
      value: import.meta.env.DEV ? 24.02 : 0,
      payment_method: import.meta.env.DEV ? 'pix' : '',
      type: import.meta.env.DEV ? 'donation' : '',
      status: import.meta.env.DEV ? 'finished' : '',
    },
    resolver: yupResolver(schema),
  });

  const {
    control,
    register,
    handleSubmit,
    setValue,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(
    async (data: DonationFormData) => {
      const formatted_data = { ...data, user_id: data.user_id?.value || null };

      let value = String(data.value);
      if (value.length >= 8) {
        value = value.replace('.', '').replace(',', '.');
      } else {
        value = value.replace(',', '.');
      }
      formatted_data.value = Number.parseFloat(value);

      await toast.promise(
        api.post('/donations', formatted_data),
        {
          pending: 'Salvando doação / dízimo',
          success: 'Doação / dízimo salvo com sucesso',
          error: 'Falha ao salvar doação / dízimo',
        },
        {
          autoClose: 2000,
          onClose: () => {
            navigate('/donations');
          },
        },
      );
    },
    [navigate],
  );

  useEffect(() => {
    async function loadUsers() {
      const response = await api.get('/users');
      if (response.data) {
        setUsers(
          response.data.map((u: User) => ({ id: u.id, name: u.full_name })),
        );
      }
    }
    loadUsers();
  }, []);

  useEffect(() => {
    if (has_user) {
      setValue('user_name', import.meta.env.DEV ? 'Thiago Vieira da Paz' : '');
      setValue(
        'user_address',
        import.meta.env.DEV
          ? 'Rua Delmiro de Farias, 150, APTO 301, Jardim América, Fortaleza, CE, 00000-000'
          : '',
      );
    } else {
      setValue('user_id', undefined);
      setValue('user', undefined);
    }
  }, [has_user, setValue]);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Heading as="h1" size="lg">
        Doação / Dízimo
      </Heading>
      <Divider my="6" borderColor="gray.700" />

      <Flex
        as="form"
        w="100%"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <VStack width="100%" spacing="8">
          <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
            <FormProvider {...form}>
              <Flex gap="2" direction="column">
                {has_user ? (
                  <CustomSelect
                    name="user_id"
                    label="Usuário"
                    options={users}
                    error={errors.user_id}
                    placeholder="Selecione o usuário"
                    noOptionsMessage={() => 'Nenhum dado encontrado'}
                  />
                ) : (
                  <>
                    <TextInput
                      placeholder="Nome completo"
                      label="Usuário"
                      {...register('user_name')}
                      error={errors.user_name}
                    />
                    <TextArea
                      placeholder="Endereço completo"
                      label="Endereço completo"
                      {...register('user_address')}
                      error={errors.user_address}
                    />
                    <Text color="GrayText">
                      <Text as="span" color="red.500">
                        *
                      </Text>
                      Rua Abcde, 139, Altos, Bairro da Liberdade, Cidade, UF,
                      00000-000
                    </Text>
                  </>
                )}
                <Checkbox
                  onChange={(e) => setHasUser(!e.currentTarget.checked)}
                >
                  Informar usuário manualmente
                </Checkbox>
              </Flex>
            </FormProvider>
          </SimpleGrid>
          <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
            <FormattedInput
              name="value"
              label="Valor"
              placeholder="Valor"
              error={errors.value}
              control={control}
            />
            <Select
              label="Método de pagamento"
              options={payment_types}
              placeholder="Selecione o método de pagamento"
              error={errors.payment_method}
              {...register('payment_method')}
            />
            <Select
              label="Tipo"
              options={donation_types}
              placeholder="Selecione o tipo"
              error={errors.type}
              {...register('type')}
            />
            <Select
              label="Status"
              options={status_types}
              placeholder="Selecione o status"
              error={errors.status}
              {...register('status')}
            />
          </SimpleGrid>
        </VStack>

        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button as={NavLink} to="/donations" colorScheme="whiteAlpha">
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Create;
