/* eslint-disable import/no-duplicates */
import { useCallback, useEffect, useRef, useState } from 'react';
import {
  Flex,
  Heading,
  Divider,
  Button,
  Text,
  useDisclosure,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  FormControl,
  FormLabel,
  Icon,
  Select,
} from '@chakra-ui/react';
import { formatDistanceToNow, format, parseISO } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import { toast } from 'react-toastify';
import { NavLink } from 'react-router-dom';
import {
  defaultStaticRanges,
  defaultInputRanges,
  DateRangePicker,
  Range,
} from 'react-date-range';
import { FaFilter, FaCloudDownloadAlt, FaTrash } from 'react-icons/fa';
import fileDownload from 'js-file-download';

import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import * as locales from 'react-date-range/dist/locale';

import api from '../../services/api';
import { regions } from '../../utils/regions';

import DataTable from '../../components/DataTable';

interface User {
  id: string;
  full_name: string;
  full_address?: string;
}

interface Donation {
  id: string;
  value: number;
  type: string;
  payment_method: string;
  status: string;
  user?: User;
  user_name?: string;
  user_address?: string;
  created_at: Date;
}

interface ActionsButtonsProps {
  id: string;
}

interface FilterComponentProps {
  state: any;
  onChangeState(item: Range[]): void;
  type: string;
  onChangeType(item: string): void;
  region: string;
  onChangeRegion(item: string): void;
}

const status: any = { finished: 'Finalizado', created: 'Criado' };
const donations_type: any = {
  donation: 'Doação',
  tithe: 'Dízimo',
  candle: 'Vela',
  cord: 'Cordão',
};

const types = [
  { id: 'tithe', name: 'Dízimo' },
  { id: 'donation', name: 'Doação' },
  { id: 'candle', name: 'Vela' },
  { id: 'cord', name: 'Cordão' },
];

const FilterComponent: React.FC<FilterComponentProps> = ({
  state,
  onChangeState,
  type,
  onChangeType,
  region,
  onChangeRegion,
}) => {
  const [openReport, setOpenReport] = useState(false);

  const handleGenerateReport = useCallback(async () => {
    try {
      // if (type) {
      const response = await api.post(
        '/reports/donations',
        {
          start_date: state[0].startDate ?? undefined,
          end_date: state[0].endDate ?? undefined,
          type,
          region,
        },
        {
          responseType: 'blob',
        },
      );

      fileDownload(response.data, `relatorio-usuarios.pdf`);
      // } else {
      //   toast.error('Selecione o Tipo');
      // }
    } catch (error) {
      //
    }
  }, [state, region, type]);

  const handleFilter = () => {
    setOpenReport((s) => !s);
  };

  const removeFilter = () => {
    setOpenReport((s) => !s);
    onChangeType('');
    onChangeRegion('');
    onChangeState([
      { key: 'selection', startDate: undefined, endDate: undefined },
    ]);
  };

  const countFilters = useCallback(() => {
    let f = 0;
    if (type) {
      f += 1;
    }

    if (region) {
      f += 1;
    }

    if (state[0].endDate !== undefined) {
      f += 1;
    }

    return f;
  }, [type, region, state]);

  const staticRanges = defaultStaticRanges.map((r) => {
    const new_labels: any = {
      Today: 'Hoje',
      Yesterday: 'Ontem',
      'This Week': 'Esta semana',
      'Last Week': 'Semana passada',
      'This Month': 'Este mês',
      'Last Month': 'Mês passado',
    };

    return { ...r, label: new_labels[r.label!] };
  });

  const inputRanges = defaultInputRanges.map((i) => {
    const new_labels: any = {
      'days up to today': 'Dias antes',
      'days starting today': 'Dias depois',
    };
    return { ...i, label: new_labels[i.label!] };
  });

  return (
    <Flex>
      <Button
        mr={2}
        colorScheme="yellow"
        onClick={() => setOpenReport((s) => !s)}
        leftIcon={<Icon as={FaFilter} />}
      >
        FILTROS ({countFilters()})
      </Button>

      <Modal
        size="full"
        isOpen={openReport}
        onClose={() => setOpenReport((s) => !s)}
      >
        <ModalOverlay />
        <ModalContent bg="gray.800">
          <ModalHeader py={1}>RELATÓRIOS</ModalHeader>
          <ModalCloseButton />
          <ModalBody py={0}>
            <Flex flex={1} maxW="850px" direction="column">
              <FormControl py={1} mb={0}>
                <FormLabel>Tipo</FormLabel>
                <Select
                  onChange={(e) => {
                    onChangeType(e.target.value);
                  }}
                  value={type}
                  placeholder="Selecione o tipo"
                >
                  {types.map((r) => (
                    <option key={r.id} value={r.id}>
                      {r.name}
                    </option>
                  ))}
                </Select>
              </FormControl>
              <FormControl py={1} mb={2}>
                <FormLabel>Estado</FormLabel>
                <Select
                  onChange={(e) => {
                    onChangeRegion(e.target.value);
                  }}
                  value={region}
                  placeholder="Estado"
                >
                  {regions.map((r) => (
                    <option key={r.id} value={r.id}>
                      {r.name}
                    </option>
                  ))}
                </Select>
              </FormControl>
              <DateRangePicker
                inputRanges={inputRanges}
                staticRanges={staticRanges}
                onChange={({ selection }) => {
                  onChangeState([selection]);
                }}
                moveRangeOnFirstSelection={false}
                months={2}
                ranges={state}
                direction="horizontal"
                locale={locales.pt}
              />
            </Flex>
          </ModalBody>
          <ModalFooter>
            <Flex w="100%" gap={2} justify="flex-end">
              <Button
                leftIcon={<Icon as={FaFilter} />}
                colorScheme="blue"
                onClick={handleFilter}
              >
                FILTRAR
              </Button>
              <Button
                leftIcon={<Icon as={FaCloudDownloadAlt} />}
                colorScheme="green"
                onClick={handleGenerateReport}
              >
                BAIXAR
              </Button>
              <Button
                leftIcon={<Icon as={FaTrash} />}
                colorScheme="red"
                mr={3}
                onClick={removeFilter}
              >
                FECHAR E LIMPAR
              </Button>
            </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

const Donations = () => {
  const modalRef = useRef(null);
  const { onClose, onOpen, isOpen } = useDisclosure();

  const [donations, setDonations] = useState<Donation[]>([]);
  const [loading, setLoading] = useState(false);
  const [preview, setPreview] = useState<Donation | undefined>(undefined);
  const [deleteItemId, setDeleteItemId] = useState<string | null>(null);

  const [state, setState] = useState<Range[]>([
    {
      startDate: undefined,
      endDate: undefined,
      key: 'selection',
    },
  ]);

  const [type, setType] = useState('');
  const [region, setRegion] = useState('');

  const handleDeleteRows = useCallback(async (ids: string[]) => {
    const deleteRows = ids.map(async (id) => {
      await api.delete(`/donations/${id}`);
    });
    await Promise.all([deleteRows]);

    toast.success('Doações / Dízimos removidos', { autoClose: 2000 });

    setDonations((oldState) =>
      oldState.filter((item) => !ids.includes(item.id)),
    );
  }, []);

  const handleDelete = useCallback(async () => {
    try {
      toast.promise(api.delete(`/donations/${deleteItemId}`), {
        pending: 'Removendo doação / dízimo',
        success: 'doação / dízimo removido(a)',
        error: 'Erro ao remover doação / dízimo',
      });
      setDonations((oldState) => oldState.filter((s) => s.id !== deleteItemId));
    } catch (error) {
      // alert(error);
    }
    onClose();
    setDeleteItemId(null);
  }, [deleteItemId, onClose]);

  const formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
  });

  useEffect(() => {
    async function loadDonations() {
      setLoading(true);
      const response = await api.get(
        `/donations?type=${type}&start_date=${
          state[0].startDate ? format(state[0].startDate!, 'yyyy-MM-dd') : ''
        }&end_date=${
          state[0].endDate ? format(state[0].endDate!, 'yyyy-MM-dd') : ''
        }&region=${region}`,
      );

      if (response.data) {
        setDonations(response.data);
      }
    }
    loadDonations();
  }, [type, region, state]);

  const ActionsButtons: React.FC<ActionsButtonsProps> = ({ id }) => {
    const handleView = () => {
      setPreview(donations.find((n) => n.id === id));
    };

    return (
      <Flex gap="2">
        <Button
          onClick={handleView}
          size="sm"
          colorScheme="blue"
          variant="solid"
        >
          Visualizar
        </Button>
        <Button
          onClick={() => {
            setDeleteItemId(id);
            onOpen();
          }}
          size="sm"
          colorScheme="red"
          variant="solid"
        >
          Remover
        </Button>
      </Flex>
    );
  };

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex w="100%" justify="space-between" align="flex-start">
        <Heading as="h1" size="lg">
          Doações e Dízimos ({donations.length})
        </Heading>
        <Button
          as={NavLink}
          to="/donations/create"
          size="md"
          fontSize="md"
          colorScheme="pink"
        >
          Nova Doação / Dízimo
        </Button>
      </Flex>
      <Divider my="6" borderColor="gray.700" />
      <DataTable<Donation>
        data={donations}
        columns={[
          {
            accessorKey: 'user',
            accessorFn: (row) => `${row.user?.full_name || row.user_name}`,
            cell: (info) => info.getValue(),
            header: () => 'Nome',
            footer: () => 'Nome',
          },
          {
            accessorKey: 'value',
            cell: (info) => formatter.format(info.getValue()),
            header: () => 'Valor',
            footer: () => 'Valor',
          },
          {
            accessorKey: 'created_at',
            cell: (info) =>
              formatDistanceToNow(parseISO(info.getValue()), {
                addSuffix: true,
                locale: ptBR,
              }),
            header: () => 'Data de criação',
            footer: () => 'Data de criação',
          },
          {
            accessorKey: 'id',
            cell: (info) => <ActionsButtons id={info.getValue()} />,
            header: () => 'Ações',
            footer: () => 'Ações',
          },
        ]}
        isLoading={loading}
        deleteFn={handleDeleteRows}
        filterComponent={
          <FilterComponent
            state={state}
            onChangeState={(item) => setState(item)}
            type={type}
            onChangeType={(item) => setType(item)}
            region={region}
            onChangeRegion={(item) => setRegion(item)}
          />
        }
      />
      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        leastDestructiveRef={modalRef}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Remover Doação / Dízimo
            </AlertDialogHeader>

            <AlertDialogBody>
              Você tem certeza que deseja remover ?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button onClick={onClose} color="gray.900 ">
                Cancelar
              </Button>
              <Button colorScheme="red" onClick={handleDelete} ml={3}>
                Remover
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>

      <Modal isOpen={!!preview} onClose={() => setPreview(undefined)}>
        <ModalOverlay />
        <ModalContent bg="gray.800">
          <ModalHeader>Doação / Dízimo</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {preview?.value && (
              <Text>
                Valor: <b>{formatter.format(preview.value)}</b>
              </Text>
            )}
            <Text>
              Nome: <b>{preview?.user?.full_name || preview?.user_name}</b>
            </Text>
            <Text>
              Endereço:{' '}
              <b>
                {preview?.user
                  ? preview?.user.full_address || '-'
                  : preview?.user_address}
              </b>
            </Text>
            {preview?.type && (
              <Text>
                Tipo: <b>{donations_type[preview.type]}</b>
              </Text>
            )}
            {preview?.status && (
              <Text>
                Status: <b>{status[preview.status]}</b>
              </Text>
            )}

            {preview?.created_at && (
              <Text mt={5} fontSize="xs">
                {format(
                  new Date(preview.created_at),
                  "dd 'de' MMMM 'de' yyyy",
                  {
                    locale: ptBR,
                  },
                )}
              </Text>
            )}
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme="yellow"
              mr={3}
              onClick={() => setPreview(undefined)}
            >
              Fechar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

export default Donations;
