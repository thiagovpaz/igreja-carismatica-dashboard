import { useCallback, useEffect } from 'react';
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { useForm, FormProvider } from 'react-hook-form';
import { NavLink, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { TextInput } from '../../components/Form/TextInput';
import { TextArea } from '../../components/Form/TextArea';
import { Select } from '../../components/Form/Select';
import { Editor } from '../../components/Form/Editor';

import api from '../../services/api';

interface NotificationFormData {
  title: string;
  message: string;
  type: 'email' | 'mobile';
}

const schema = yup.object({
  title: yup.string().required('Campo Obrigatório'),
  message: yup.string().required('Campo Obrigatório'),
  type: yup.string().required('Campo Obrigatório'),
});

const Create = () => {
  const navigate = useNavigate();

  const form = useForm<NotificationFormData>({
    defaultValues: {
      title: 'Mensagem teste',
      message: 'Envio de teste, isto é apenas um teste.',
      type: 'email',
    },
    resolver: yupResolver(schema),
  });

  const {
    watch,
    setValue,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(
    async (data: NotificationFormData) => {
      await toast.promise(
        api.post('/notifications', data),
        {
          pending: 'Enviando notificação',
          success: 'Notificação enviada com sucesso',
          error: 'Falha ao enviar notificação',
        },
        {
          autoClose: 2000,
          onClose: () => {
            navigate('/notifications');
          },
        },
      );
    },
    [navigate],
  );

  const type = watch('type');
  const editorContent = watch('message');

  useEffect(() => {
    if (type === 'mobile') {
      setValue('message', editorContent.replace(/<\/?[^>]+(>|$)/g, ''));
    }
  }, [type, setValue, editorContent]);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex
        as="form"
        w="100%"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Heading as="h1" size="lg">
          Enviar notificação
        </Heading>
        <Divider my="6" borderColor="gray.700" />
        <VStack width="100%" spacing="8">
          <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
            <TextInput
              label="Título"
              placeholder="Título"
              error={errors.title}
              {...register('title')}
            />
          </SimpleGrid>
          {type !== 'email' && (
            <TextArea
              label="Mensagem"
              size="xl"
              placeholder="Mensagem"
              error={errors.message}
              {...register('message')}
            />
          )}
          {type === 'email' && (
            <FormProvider {...form}>
              <Editor
                label="Mensagem"
                error={errors.message}
                {...register('message')}
              />
            </FormProvider>
          )}
          <Select
            label="Enviar para"
            placeholder="Selecione"
            options={[
              { id: 'mobile', name: 'Aplicativo' },
              { id: 'email', name: 'E-mail' },
            ]}
            error={errors.type}
            {...register('type')}
          />
        </VStack>

        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button as={NavLink} to="/notifications" colorScheme="whiteAlpha">
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Create;
