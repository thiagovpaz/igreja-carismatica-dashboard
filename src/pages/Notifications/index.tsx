/* eslint-disable import/no-duplicates */
import { useRef, useState, useEffect, useCallback } from 'react';
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  Divider,
  Flex,
  Heading,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { formatDistanceToNow, format, parseISO } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import { NavLink } from 'react-router-dom';

import api from '../../services/api';

import DataTable from '../../components/DataTable';

interface Notification {
  id: string;
  title: string;
  message: string;
  created_at: Date;
}

interface ActionsButtonsProps {
  id: string;
}

const Notifications = () => {
  const modalRef = useRef(null);

  const { onClose, onOpen, isOpen } = useDisclosure();

  const [notifications, setNotifications] = useState<Notification[]>([]);
  const [loading, setLoading] = useState(true);
  const [deleteItemId, setDeleteItemId] = useState<string | null>(null);

  const [preview, setPreview] = useState<Notification | undefined>();

  const handleDeleteRows = useCallback(async (ids: string[]) => {
    const deleteRows = ids.map(async (id) => {
      await api.delete(`/notifications/${id}`);
    });

    await toast.promise(Promise.all(deleteRows), {
      pending: 'Removendo notificações, aguarde',
      success: 'Notificações removidas',
      error: 'Erro ao remover notificações',
    });

    setNotifications((state) => state.filter((item) => !ids.includes(item.id)));
  }, []);

  const handleDelete = useCallback(async () => {
    try {
      toast.promise(api.delete(`/notifications/${deleteItemId}`), {
        pending: 'Removendo notificação',
        success: 'Notificação removida',
        error: 'Erro ao remover notificação',
      });
      setNotifications((state) => state.filter((s) => s.id !== deleteItemId));
    } catch (error) {
      // alert(error);
    }
    onClose();
    setDeleteItemId(null);
  }, [deleteItemId, onClose]);

  useEffect(() => {
    async function loadNotifications() {
      const response = await api.get('/notifications');

      if (response.data) {
        setNotifications(response.data);
        setLoading(false);
      }
    }
    loadNotifications();
  }, []);

  const ActionsButtons: React.FC<ActionsButtonsProps> = ({ id }) => {
    const handleView = () => {
      setPreview(notifications.find((n) => n.id === id));
    };

    return (
      <Flex gap="2">
        <Button
          onClick={handleView}
          size="sm"
          colorScheme="blue"
          variant="solid"
        >
          Visualizar
        </Button>
        <Button
          onClick={() => {
            setDeleteItemId(id);
            onOpen();
          }}
          size="sm"
          colorScheme="red"
          variant="solid"
        >
          Remover
        </Button>
      </Flex>
    );
  };

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex w="100%" justify="space-between" align="flex-start">
        <Heading as="h1" size="lg">
          Notificações ({notifications.length})
        </Heading>
        <Button
          as={NavLink}
          to="/notifications/create"
          size="md"
          fontSize="md"
          colorScheme="pink"
        >
          Nova Notificação
        </Button>
      </Flex>
      <Divider my="6" borderColor="gray.700" />

      <DataTable<Notification>
        data={notifications}
        columns={[
          {
            accessorKey: 'title',
            cell: (info) => info.getValue(),
            header: () => 'Título',
            footer: () => 'Título',
          },
          {
            accessorKey: 'created_at',
            cell: (info) =>
              formatDistanceToNow(parseISO(info.getValue()), {
                addSuffix: true,
                locale: ptBR,
              }),
            header: () => 'Data de criação',
            footer: () => 'Data de criação',
          },
          {
            accessorKey: 'id',
            cell: (info) => <ActionsButtons id={info.getValue()} />,
            header: () => 'Ações',
            footer: () => 'Ações',
          },
        ]}
        isLoading={loading}
        deleteFn={handleDeleteRows}
      />
      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        leastDestructiveRef={modalRef}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Remover Notificação
            </AlertDialogHeader>

            <AlertDialogBody>
              Você tem certeza que deseja remover ?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button onClick={onClose} color="gray.900 ">
                Cancelar
              </Button>
              <Button colorScheme="red" onClick={handleDelete} ml={3}>
                Remover
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>

      <Modal isOpen={!!preview} onClose={() => setPreview(undefined)}>
        <ModalOverlay />
        <ModalContent bg="gray.800">
          <ModalHeader>{preview?.title}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {preview?.message && (
              <Box
                p={4}
                dangerouslySetInnerHTML={{ __html: preview.message }}
              />
            )}

            {preview?.created_at && (
              <Text mt={5} fontSize="xs">
                {format(
                  new Date(preview.created_at),
                  "dd 'de' MMMM 'de' yyyy",
                  {
                    locale: ptBR,
                  },
                )}
              </Text>
            )}
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme="yellow"
              mr={3}
              onClick={() => setPreview(undefined)}
            >
              Fechar
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

export default Notifications;
