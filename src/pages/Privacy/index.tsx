/* eslint-disable import/no-duplicates */
import { useEffect, useState } from 'react';
import { Box, Container, Text } from '@chakra-ui/react';
import { format } from 'date-fns';
import { ptBR } from 'date-fns/locale';

import api from '../../services/api';

import 'react-quill/dist/quill.snow.css';

interface IPrivacy {
  title: string;
  subtitle: string;
  content: string;
  created_at: Date;
}

const Privacy = () => {
  const [loading, setLoading] = useState(true);
  const [privacy, setPrivacy] = useState<IPrivacy | null>(null);

  const appNameHighLight = (content: string) => {
    // return content.replaceAll(
    //   'Trezena de São Miguel Arcanjo',
    //   '<b>Trezena de São Miguel Arcanjo</b>',
    // );
    return content;
  };

  useEffect(() => {
    async function loadPrivacy() {
      try {
        const content_id = 'd8a2563e-cbe8-49f0-88d9-daeddb46d31a';
        const response = await api.get(`/pages/${content_id}`);

        if (response.data) {
          setPrivacy(response.data);
        }
      } catch (error) {
        //
      } finally {
        setLoading(false);
      }
    }

    loadPrivacy();
  }, []);

  return (
    <Container maxW="3xl" p={3}>
      {!loading && privacy && (
        <>
          <Text fontSize="2xl">{privacy.title}</Text>
          <Text fontSize="lg" mb={2}>
            {privacy.subtitle}
          </Text>
          <div
            className="ql-editor"
            dangerouslySetInnerHTML={{
              __html: appNameHighLight(privacy.content),
            }}
          />
          <Box py={3}>
            <Text as="b">
              Esta política é efetiva a partir de{' '}
              {format(
                new Date(privacy.created_at),
                "dd 'de' LLLL 'de' yyyy 'às' hh:mm'.'",
                {
                  locale: ptBR,
                },
              )}
            </Text>
          </Box>
        </>
      )}
    </Container>
  );
};

export default Privacy;
