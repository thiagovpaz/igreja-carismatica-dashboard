import { useCallback, useEffect } from 'react';
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { NavLink } from 'react-router-dom';
import { useForm, FormProvider } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import api from '../services/api';

import { TextInput } from '../components/Form/TextInput';
import { Checkbox } from '../components/Form/Checkbox';

interface ConfigFormData {
  whatsapp_number: string;
  donation_pix: string;
  donation_url: string;
  bible_url: string;
  live_id: string;
  live_url: string;
  live_status: boolean;
  radio_ios_url: string;
  radio_android_url: string;
  share_ios_url: string;
  share_android_url: string;
  store_url: string;
}

const schema = yup.object({
  whatsapp_number: yup.string().required('Campo Obrigatório'),
});

const Settings: React.FC = () => {
  const form = useForm<ConfigFormData>({ resolver: yupResolver(schema) });

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(async (data: ConfigFormData) => {
    await toast.promise(
      api.put('/settings', data),
      {
        pending: 'Atualizando configurações',
        success: 'Configurações atualizadas',
        error: 'Falha ao atualizar configurações',
      },
      {
        autoClose: 2000,
      },
    );
  }, []);

  useEffect(() => {
    async function loadSettings() {
      const response = await api.get('/settings');

      if (response.data) {
        reset({ ...response.data, live_status: !!response.data.live_status });
      }
    }
    loadSettings();
  }, [reset]);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex
        as="form"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Heading as="h1" size="lg">
          Configurações
        </Heading>
        <Divider my="6" borderColor="gray.700" />

        <VStack width="100%" spacing="8">
          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            <Heading as="h1" size="md" mb={4}>
              Doações
            </Heading>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Pix de doação"
                placeholder="Pix de doação"
                error={errors.donation_pix}
                {...register('donation_pix')}
              />
              <TextInput
                label="Link de doação"
                placeholder="Link de doação"
                error={errors.donation_url}
                {...register('donation_url')}
              />
            </SimpleGrid>
          </Flex>
          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Link da bíblia"
                placeholder="Link da bíblia"
                error={errors.bible_url}
                {...register('bible_url')}
              />
            </SimpleGrid>
          </Flex>

          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            <Heading as="h1" size="md" mb={4}>
              Live
            </Heading>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="ID da live"
                placeholder="ID da live"
                error={errors.live_id}
                {...register('live_id')}
              />
              <TextInput
                label="Link de redirecionamento da live"
                placeholder="Link de redirecionamento da live"
                error={errors.live_url}
                {...register('live_url')}
              />
              <Flex mt="8">
                <FormProvider {...form}>
                  <Checkbox
                    size="lg"
                    colorScheme="pink"
                    {...register('live_status')}
                  >
                    Ao Vivo
                  </Checkbox>
                </FormProvider>
              </Flex>
            </SimpleGrid>
          </Flex>
          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            <Heading as="h1" size="md" mb={4}>
              Rádio
            </Heading>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Link da rádio (IOS)"
                placeholder="Link da rádio (IOS)"
                error={errors.radio_ios_url}
                {...register('radio_ios_url')}
              />
              <TextInput
                label="Link da rádio (ANDROID)"
                placeholder="Link da rádio (ANDROID)"
                error={errors.radio_android_url}
                {...register('radio_android_url')}
              />
            </SimpleGrid>
          </Flex>
          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            <Heading as="h1" size="md" mb={4}>
              Aplicativo
            </Heading>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Link do app (IOS)"
                placeholder="Link do app (IOS)"
                error={errors.share_ios_url}
                {...register('share_ios_url')}
              />
              <TextInput
                label="Link do app (ANDROID)"
                placeholder="Link do app (ANDROID)"
                error={errors.share_android_url}
                {...register('share_android_url')}
              />
            </SimpleGrid>
          </Flex>

          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            {/* <Heading as="h1" size="md" mb={4}>
              Live
            </Heading> */}
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Link da loja"
                placeholder="Link da loja"
                error={errors.store_url}
                {...register('store_url')}
              />
            </SimpleGrid>
          </Flex>
          <Flex
            direction="column"
            w="100%"
            border="1px"
            borderColor="gray.700"
            borderRadius={4}
            p={4}
          >
            {/* <Heading as="h1" size="md" mb={4}>
              Live
            </Heading> */}
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Número do whatsApp"
                placeholder="Número do whatsApp"
                error={errors.whatsapp_number}
                {...register('whatsapp_number')}
              />
            </SimpleGrid>
          </Flex>
        </VStack>

        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button as={NavLink} to="/dashboard" colorScheme="whiteAlpha">
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Settings;
