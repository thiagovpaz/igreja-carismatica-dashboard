import { useCallback } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Button, Flex, Image, Stack, Text } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { useAuth } from '../hooks/auth';

import { TextInput } from '../components/Form/TextInput';

import logo from '../assets/images/logo.png';

interface SigninFormData {
  email: string;
  password: string;
}

const SignIn = () => {
  const schema = yup.object({
    email: yup.string().email('E-mail Inválido').required('Campo Obrigatório'),
    password: yup.string().required('Campo Obrigatório'),
  });

  const { signIn } = useAuth();

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<SigninFormData>({
    defaultValues: {
      // email: 'admin@admin.com',
      // password: 'secret',
    },
    resolver: yupResolver(schema),
  });

  const handleSignIn = useCallback(
    async ({ email, password }: SigninFormData) => {
      try {
        toast.promise(signIn({ email, password }), {
          pending: 'Realizando login',
          success: 'Login efetuado com sucesso',
          error: 'E-mail ou senha incorretos',
        });
      } catch (error) {
        toast('Erro desconhecido, por favor tente novamente');
      }
    },
    [signIn],
  );

  return (
    <Flex w="100%" h="100vh" justify="center" align="center">
      <Flex
        as="form"
        width="100%"
        maxWidth="360"
        bg="gray.800"
        p="8"
        borderRadius="8"
        flexDirection="column"
        onSubmit={handleSubmit(handleSignIn)}
      >
        <Stack spacing={4}>
          <Image src={logo} w={135} alignSelf="center" />

          <TextInput
            type="email"
            label="E-mail"
            placeholder="Seu E-mail"
            error={errors.email}
            {...register('email')}
          />
          <TextInput
            type="password"
            label="Senha"
            placeholder="Sua senha"
            error={errors.password}
            {...register('password')}
          />
          <Button
            type="submit"
            size="lg"
            isLoading={isSubmitting}
            colorScheme="blue"
          >
            Entrar
          </Button>
          <Link to="/account/password/forgot">Esqueci minha senha</Link>
        </Stack>
      </Flex>
    </Flex>
  );
};

export default SignIn;
