import { useCallback, useEffect, useState } from 'react';
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import api from '../../services/api';

import { Select } from '../../components/Form/Select';
import { TextArea } from '../../components/Form/TextArea';
import { AvatarUpload } from '../../components/Form/AvatarUpload';
import { TextInput } from '../../components/Form/TextInput';

import { regions } from '../../utils/regions';

interface TestimonyFormData {
  name: string;
  city: string;
  region: string;
  avatar_file: string;
  testimony: string;
}

const schema = yup.object({
  name: yup.string().required('Campo Obrigatório'),
  city: yup.string().required('Campo Obrigatório'),
  region: yup.string().required('Campo Obrigatório'),
  testimony: yup.string().required('Campo Obrigatório'),
});

const Edit = () => {
  const navigate = useNavigate();
  const { id } = useParams();

  const form = useForm<TestimonyFormData>({
    defaultValues: {},
    resolver: yupResolver(schema),
  });

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(
    async (data: TestimonyFormData) => {
      await toast.promise(
        api.put(`/testimonies/${id}`, data),
        {
          pending: 'Salvando testemunho',
          success: 'Testemunho atualizado com sucesso',
          error: 'Falha ao salvar testemunho',
        },
        {
          autoClose: 2000,
          onClose: () => {
            navigate('/testimonies');
          },
        },
      );
    },
    [navigate, id],
  );

  useEffect(() => {
    async function loadTestimony() {
      const response = await api.get(`/testimonies/${id}`);
      if (response.data) {
        reset(response.data);
      }
    }
    loadTestimony();
  }, [id, reset]);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Heading as="h1" size="lg">
        Testemunho
      </Heading>
      <Divider my="6" borderColor="gray.700" />

      <Flex
        as="form"
        w="100%"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <VStack width="100%" spacing="8">
          <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
            <FormProvider {...form}>
              <AvatarUpload {...register('avatar_file')} />
            </FormProvider>
            <TextInput
              label="Nome"
              placeholder="Digite o nome"
              error={errors.name}
              {...register('name')}
            />
          </SimpleGrid>
          <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
            <TextInput
              label="Cidade"
              placeholder="Digite a cidade"
              error={errors.city}
              {...register('city')}
            />
            <Select
              label="Estado"
              placeholder="Selecione o estado"
              options={regions}
              error={errors.region}
              {...register('region')}
            />
          </SimpleGrid>
          <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
            <TextArea
              rows={10}
              label="Testemunho"
              placeholder="Digite o testemunho"
              error={errors.testimony}
              {...register('testimony')}
            />
          </SimpleGrid>
        </VStack>

        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button as={NavLink} to="/testimonies" colorScheme="whiteAlpha">
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Edit;
