/* eslint-disable import/no-duplicates */
import React, { useState, useEffect, useRef, useCallback } from 'react';
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Avatar,
  Button,
  Divider,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Icon,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Text,
  useDisclosure,
} from '@chakra-ui/react';
import { NavLink, useNavigate } from 'react-router-dom';
import { format, parseISO } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import { toast } from 'react-toastify';
import {
  defaultStaticRanges,
  defaultInputRanges,
  DateRangePicker,
  Range,
} from 'react-date-range';
import { FaFilter, FaTrash } from 'react-icons/fa';

import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import * as locales from 'react-date-range/dist/locale';

import DataTable from '../../components/DataTable';

import api from '../../services/api';
import { regions } from '../../utils/regions';

interface FilterComponentProps {
  state: any;
  onChangeState(item: Range[]): void;
  region: string;
  onChangeRegion(item: string): void;
}

interface ActionsButtonsProps {
  id: string;
}

interface Testimony {
  id: string;
  name: string;
  region: string;
  city: string;
  testimony: string;
  avatar_url?: string;
  created_at: Date;
}

const FilterComponent: React.FC<FilterComponentProps> = ({
  state,
  onChangeState,
  region,
  onChangeRegion,
}) => {
  const [openReport, setOpenReport] = useState(false);

  const handleGenerateReport = useCallback(() => {
    // console.log(state, region);
  }, [state, region]);

  const handleFilter = () => {
    setOpenReport((s) => !s);
  };

  const removeFilter = () => {
    setOpenReport((s) => !s);
    onChangeRegion('');
    onChangeState([
      { key: 'selection', startDate: undefined, endDate: undefined },
    ]);
  };

  const countFilters = useCallback(() => {
    let f = 0;
    if (region) {
      f += 1;
    }

    if (state[0].endDate !== undefined) {
      f += 1;
    }

    return f;
  }, [region, state]);

  const staticRanges = defaultStaticRanges.map((r) => {
    const new_labels: any = {
      Today: 'Hoje',
      Yesterday: 'Ontem',
      'This Week': 'Esta semana',
      'Last Week': 'Semana passada',
      'This Month': 'Este mês',
      'Last Month': 'Mês passado',
    };

    return { ...r, label: new_labels[r.label!] };
  });

  const inputRanges = defaultInputRanges.map((i) => {
    const new_labels: any = {
      'days up to today': 'Dias antes',
      'days starting today': 'Dias depois',
    };
    return { ...i, label: new_labels[i.label!] };
  });

  return (
    <Flex>
      <Button
        mr={2}
        colorScheme="yellow"
        onClick={() => setOpenReport((s) => !s)}
        leftIcon={<Icon as={FaFilter} />}
      >
        FILTROS ({countFilters()})
      </Button>

      <Modal
        size="full"
        isOpen={openReport}
        onClose={() => setOpenReport((s) => !s)}
      >
        <ModalOverlay />
        <ModalContent bg="gray.800">
          <ModalHeader>RELATÓRIOS</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex flex={1} maxW="850px" direction="column">
              <FormControl py={2} mb={5}>
                <FormLabel>Estado</FormLabel>
                <Select
                  onChange={(e) => {
                    onChangeRegion(e.target.value);
                  }}
                  value={region}
                  placeholder="Estado"
                >
                  {regions.map((r) => (
                    <option key={r.id} value={r.id}>
                      {r.name}
                    </option>
                  ))}
                </Select>
              </FormControl>
              <DateRangePicker
                inputRanges={inputRanges}
                staticRanges={staticRanges}
                onChange={({ selection }) => {
                  onChangeState([selection]);
                }}
                moveRangeOnFirstSelection={false}
                months={2}
                ranges={state}
                direction="horizontal"
                locale={locales.pt}
              />
            </Flex>
          </ModalBody>
          <ModalFooter>
            <Flex w="100%" gap={2} justify="flex-end">
              <Button
                leftIcon={<Icon as={FaFilter} />}
                colorScheme="blue"
                onClick={handleFilter}
              >
                FILTRAR
              </Button>
              {/* <Button
                leftIcon={<Icon as={FaCloudDownloadAlt} />}
                colorScheme="green"
                onClick={handleGenerateReport}
              >
                BAIXAR
              </Button> */}
              <Button
                leftIcon={<Icon as={FaTrash} />}
                colorScheme="red"
                mr={3}
                onClick={removeFilter}
              >
                FECHAR E LIMPAR
              </Button>
            </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

const Testimonies = () => {
  const modalRef = useRef(null);
  const { onClose, onOpen, isOpen } = useDisclosure();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);
  const [testimonies, setTestimonies] = useState<Testimony[]>([]);
  const [preview, setPreview] = useState<Testimony | undefined>(undefined);
  const [deleteItemId, setDeleteItemId] = useState<string | null>(null);

  const [state, setState] = useState<Range[]>([
    {
      startDate: undefined,
      endDate: undefined,
      key: 'selection',
    },
  ]);
  const [region, setRegion] = useState('');

  const handleDeleteRows = useCallback(async (ids: string[]) => {
    const deleteRows = ids.map(async (id) => {
      await api.delete(`/testimonies/${id}`);
    });
    await Promise.all([deleteRows]);

    toast.success('Testemunhos removidos', { autoClose: 2000 });

    setTestimonies((oldState) =>
      oldState.filter((item) => !ids.includes(item.id)),
    );
  }, []);

  const handleDelete = useCallback(async () => {
    try {
      toast.promise(api.delete(`/testimonies/${deleteItemId}`), {
        pending: 'Removendo testemunho',
        success: 'testemunho removido(a)',
        error: 'Erro ao remover testemunho',
      });
      setTestimonies((oldState) =>
        oldState.filter((s) => s.id !== deleteItemId),
      );
    } catch (error) {
      // alert(error);
    }
    onClose();
    setDeleteItemId(null);
  }, [deleteItemId, onClose]);

  useEffect(() => {
    async function loadTestimonies() {
      try {
        setLoading(true);
        const response = await api.get(
          `/testimonies?region=${region}&start_date=${
            state[0].startDate ? format(state[0].startDate!, 'yyyy-MM-dd') : ''
          }&end_date=${
            state[0].endDate ? format(state[0].endDate!, 'yyyy-MM-dd') : ''
          }`,
        );

        if (response.data) {
          setTestimonies(response.data);
        }
      } catch (error) {
        //
      } finally {
        setLoading(false);
      }
    }
    loadTestimonies();
  }, [region, state]);

  const ActionsButtons: React.FC<ActionsButtonsProps> = ({ id }) => {
    const handleView = () => {
      setPreview(testimonies.find((n) => n.id === id));
    };

    return (
      <Flex gap="2">
        <Button
          onClick={handleView}
          size="sm"
          colorScheme="blue"
          variant="solid"
        >
          Visualizar
        </Button>
        <Button
          onClick={() => {
            setDeleteItemId(id);
            onOpen();
          }}
          size="sm"
          colorScheme="red"
          variant="solid"
        >
          Remover
        </Button>
      </Flex>
    );
  };

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex w="100%" justify="space-between" align="flex-start">
        <Heading as="h1" size="lg">
          Testemunhos ({testimonies.length})
        </Heading>
        <Button
          as={NavLink}
          to="/testimonies/create"
          size="md"
          fontSize="md"
          colorScheme="pink"
        >
          Novo Testemunho
        </Button>
      </Flex>
      <Divider my="6" borderColor="gray.700" />
      <DataTable<Testimony>
        data={testimonies}
        columns={[
          {
            accessorKey: 'name',
            accessorFn: (row) => row.name,
            cell: (info) => info.getValue(),
            header: () => 'Nome',
            footer: () => 'Nome',
          },
          {
            accessorKey: `region`,
            accessorFn: (row) => row.region,
            cell: (info) =>
              info.getValue() === 'null' ? '-' : info.getValue(),
            header: () => 'Estado',
            footer: () => 'Estado',
          },
          {
            accessorKey: 'created_at',
            cell: (info) =>
              format(parseISO(info.getValue()), 'dd/MM/yyyy', { locale: ptBR }),
            header: () => 'Data de criação',
            footer: () => 'Data de criação',
          },
          {
            accessorKey: 'id',
            cell: (info) => <ActionsButtons id={info.getValue()} />,
            header: () => 'Ações',
            footer: () => 'Ações',
          },
        ]}
        isLoading={loading}
        deleteFn={handleDeleteRows}
        filterComponent={
          <FilterComponent
            state={state}
            onChangeState={(item) => setState(item)}
            region={region}
            onChangeRegion={(item) => setRegion(item)}
          />
        }
      />
      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        leastDestructiveRef={modalRef}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Remover Testemunho
            </AlertDialogHeader>

            <AlertDialogBody>
              Você tem certeza que deseja remover ?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button onClick={onClose} color="gray.900 ">
                Cancelar
              </Button>
              <Button colorScheme="red" onClick={handleDelete} ml={3}>
                Remover
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal isOpen={!!preview} onClose={() => setPreview(undefined)}>
        <ModalOverlay />
        {preview && (
          <ModalContent bg="gray.800">
            <ModalHeader>Testemunho</ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Flex direction="column">
                <Avatar src={preview.avatar_url} size="xl" mb={4} />

                <Text>
                  Nome: <b>{preview.name}</b>
                </Text>
              </Flex>

              <br />
              <Text>
                Testemunho: <br />
                <b
                  dangerouslySetInnerHTML={{
                    __html: preview.testimony.replace(/\r?\n|\r/g, '<br>'),
                  }}
                />
              </Text>
              <br />
              <Text>
                Localização:
                <br />
                <b>
                  {preview?.city} - {preview?.region} <br />
                </b>
              </Text>

              {preview?.created_at && (
                <Text mt={5} fontSize="xs">
                  {format(
                    new Date(preview.created_at),
                    "dd 'de' MMMM 'de' yyyy",
                    {
                      locale: ptBR,
                    },
                  )}
                </Text>
              )}
            </ModalBody>

            <ModalFooter>
              <Button
                colorScheme="blue"
                mr={3}
                onClick={() => navigate(`/testimonies/edit/${preview.id}`)}
              >
                Editar
              </Button>
              <Button
                colorScheme="yellow"
                mr={3}
                onClick={() => setPreview(undefined)}
              >
                Fechar
              </Button>
            </ModalFooter>
          </ModalContent>
        )}
      </Modal>
    </Flex>
  );
};

export default Testimonies;
