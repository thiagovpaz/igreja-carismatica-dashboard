import { useEffect, useState } from 'react';
import { Box, Divider, Flex, Heading, Progress, Text } from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { useSearchParams } from 'react-router-dom';

import api from '../../services/api';

const UserAccountConfirm = () => {
  const [searchParams] = useSearchParams();

  const [confirmed, setConfirmed] = useState(false);

  useEffect(() => {
    document.title = 'Trezena de São Miguel';

    toast
      .promise(
        api.patch('/me/activate', {
          token: searchParams.get('token'),
        }),
        {
          pending: 'Confirmando conta',
          success: 'Conta confirmada com sucesso',
          error: 'Falha ao confirmar conta',
        },
        {
          autoClose: 4000,
        },
      )
      .then(() => {
        setConfirmed(true);
      });
  }, [searchParams]);

  return (
    <Flex
      width="100%"
      maxWidth="400"
      bg="gray.800"
      p="8"
      borderRadius="8"
      flexDirection="column"
      justifyContent="space-between"
    >
      <Heading
        size="lg"
        textAlign="center"
        p={0}
        mb={4}
        textTransform="uppercase"
      >
        {confirmed ? 'Conta Confirmada' : 'Confirmando Conta'}
        <Divider mt={2} />
      </Heading>
      <Box py="4" w="100%">
        {confirmed ? (
          <>
            <Text fontSize="lg" mb={6}>
              Parabéns sua conta foi confirmada com sucesso.
            </Text>
            <Text fontSize="lg">
              Agora você pode acessar sua conta através do aplicativo.
            </Text>
          </>
        ) : (
          <Progress isIndeterminate size="xs" colorScheme="blue" />
        )}
      </Box>
    </Flex>
  );
};

export default UserAccountConfirm;
