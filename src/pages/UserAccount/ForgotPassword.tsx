import { useCallback, useEffect } from 'react';
import { Stack, Image, Button, Flex } from '@chakra-ui/react';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { TextInput } from '../../components/Form/TextInput';

import api from '../../services/api';

import logo from '../../assets/images/logo.png';

const schema = yup.object({
  email: yup.string().email('E-mail inválido').required('Campo Obrigatório'),
});

interface ForgotPasswordFormData {
  email: string;
}

const ForgotPassword = () => {
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<ForgotPasswordFormData>({
    defaultValues: {
      email: '',
    },
    resolver: yupResolver(schema),
  });

  const handleForgotPassword = useCallback(
    async ({ email }: ForgotPasswordFormData) => {
      try {
        await toast.promise(
          api.post('/passwords/forgot', {
            email,
          }),
          {
            pending: 'Buscando e-mail',
            success: 'Link de rederfinição enviado com sucesso',
            error: 'Falha ao buscar e-mail',
          },
          {
            autoClose: 4000,
          },
        );
        reset();
      } catch (error: any) {
        if (error.response.data) {
          switch (error.response.data.message) {
            case 'Expired token':
              toast.error(
                'Este token expirou, Por favor solicite um novo token',
              );
              break;
            default:
              toast.error('Por favor realize uma nova solicitação.');
          }
        }
      }
    },
    [reset],
  );

  useEffect(() => {
    document.title = 'Trezena de São Miguel';
  }, []);

  return (
    <Flex
      as="form"
      width="100%"
      maxWidth="360"
      bg="gray.800"
      p="8"
      borderRadius="8"
      flexDirection="column"
      onSubmit={handleSubmit(handleForgotPassword)}
    >
      <Stack spacing={4}>
        <Image src={logo} w={135} alignSelf="center" />
        <TextInput
          type="text"
          label="E-mail"
          placeholder="Seu E-mail"
          error={errors.email}
          {...register('email')}
        />

        <Button
          type="submit"
          size="lg"
          isLoading={isSubmitting}
          colorScheme="blue"
        >
          Enviar link de redefinição
        </Button>
      </Stack>
    </Flex>
  );
};

export default ForgotPassword;
