import { useCallback, useEffect } from 'react';
import { Stack, Image, Button, Flex } from '@chakra-ui/react';
import { useSearchParams } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { toast } from 'react-toastify';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { TextInput } from '../../components/Form/TextInput';

import api from '../../services/api';

import logo from '../../assets/images/logo.png';

const schema = yup.object({
  password: yup
    .string()
    .oneOf([yup.ref('password_confirm')], 'As senhas devem coincidir')
    .required('Campo Obrigatório'),
  password_confirm: yup.string().required('Campo Obrigatório'),
});

interface ResetPasswordFormData {
  password: string;
  password_confirm: string;
}

const ResetPassword = () => {
  const [searchParams] = useSearchParams();

  const {
    reset,
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm<ResetPasswordFormData>({
    defaultValues: {
      password: '',
      password_confirm: '',
    },
    resolver: yupResolver(schema),
  });

  const handleResetPassword = useCallback(
    async ({ password, password_confirm }: ResetPasswordFormData) => {
      try {
        await toast.promise(
          api.put('/passwords/reset', {
            password,
            password_confirm,
            token: searchParams.get('token'),
          }),
          {
            pending: 'Resetando senha',
            success: 'Senha atualizada com sucesso',
            error: 'Falha ao resetar senha',
          },
          {
            autoClose: 4000,
          },
        );
        reset();
      } catch (error: any) {
        if (error.response.data) {
          switch (error.response.data.message) {
            case 'Expired token':
              toast.error(
                'Este token expirou, Por favor solicite um novo token',
              );
              break;
            default:
              toast.error('Por favor realize uma nova solicitação.');
          }
        }
      }
    },
    [searchParams, reset],
  );

  useEffect(() => {
    document.title = 'Trezena de São Miguel';
  }, []);

  return (
    <Flex
      as="form"
      width="100%"
      maxWidth="360"
      bg="gray.800"
      p="8"
      borderRadius="8"
      flexDirection="column"
      onSubmit={handleSubmit(handleResetPassword)}
    >
      <Stack spacing={4}>
        <Image src={logo} w={135} alignSelf="center" />
        <TextInput
          type="password"
          label="Senha"
          placeholder="Sua nova senha"
          error={errors.password}
          {...register('password')}
        />

        <TextInput
          type="password"
          label="Repetir senha"
          placeholder="Repita sua nova senha"
          error={errors.password_confirm}
          {...register('password_confirm')}
        />

        <Button
          type="submit"
          size="lg"
          isLoading={isSubmitting}
          colorScheme="blue"
        >
          Atualizar senha
        </Button>
      </Stack>
    </Flex>
  );
};

export default ResetPassword;
