import { useCallback } from 'react';
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import { useForm, FormProvider } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import api from '../../services/api';
import { regions } from '../../utils/regions';

import { TextInput } from '../../components/Form/TextInput';
import { Select } from '../../components/Form/Select';
import { Checkbox } from '../../components/Form/Checkbox';
import { AvatarUpload } from '../../components/Form/AvatarUpload';

interface UserFormData {
  first_name: string;
  last_name: string;
  avatar_file: string;
  email: string;
  password: string;
  phone: string;
  zip: string;
  address: string;
  number: string | number;
  district: string;
  city: string;
  region: string;
  is_active: boolean;
}

interface CreatedUserProps {
  id: string;
  name: string;
}

interface CreateProps {
  isModal?: boolean;
  onSuccess?: (user: CreatedUserProps) => void;
  onToggle?: () => void;
}

const Create: React.FC<CreateProps> = ({
  isModal = false,
  onSuccess,
  onToggle,
}) => {
  const schema = yup.object({
    first_name: yup.string().required('Campo Obrigatório'),
    last_name: yup.string().required('Campo Obrigatório'),
    email: yup.string().email('E-mail inválido').required('Campo Obrigatório'),
    phone: yup.string().required('Campo Obrigatório'),
    password: yup
      .string()
      .min(3, 'Pelo menos 3 caracteres')
      .required('Campo Obrigatório'),
  });

  const navigate = useNavigate();

  const form = useForm<UserFormData>({
    defaultValues: {
      first_name: import.meta.env.MODE === 'development' ? 'Nome' : '',
      last_name: import.meta.env.MODE === 'development' ? 'Sobrenome' : '',
      email: import.meta.env.MODE === 'development' ? 'email@email.com' : '',
      password: import.meta.env.MODE === 'development' ? 'secret' : '',
      phone: import.meta.env.MODE === 'development' ? '(85) 9999-9999' : '',
      zip: import.meta.env.MODE === 'development' ? '60420-700' : '',
      address: import.meta.env.MODE === 'development' ? '123 Main Street' : '',
      number: import.meta.env.MODE === 'development' ? 122 : '',
      district: import.meta.env.MODE === 'development' ? 'Mountain View' : '',
      city: import.meta.env.MODE === 'development' ? 'San Francisco' : '',
      region: import.meta.env.MODE === 'development' ? 'CE' : '',
      is_active: true,
    },
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(
    async (data: UserFormData) => {
      const response = await toast.promise(
        api.post('/users', data),
        {
          pending: 'Criando usuário',
          success: 'Usuário criado com sucesso',
          error: 'Falha ao criar usuário',
        },
        {
          autoClose: 2000,
          onClose: () => {
            if (!isModal) {
              navigate('/users');
            }
          },
        },
      );
      if (isModal && onSuccess) {
        const { id, full_name } = response.data;
        onSuccess({ id, name: full_name });
      }
    },
    [onSuccess, isModal, navigate],
  );

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex
        as="form"
        w="100%"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Heading as="h1" size="lg">
          Novo usuário
        </Heading>
        <Divider my="6" borderColor="gray.700" />
        <FormProvider {...form}>
          <VStack width="100%" spacing="8">
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <AvatarUpload {...register('avatar_file')} />
              <TextInput
                label="Primeiro nome"
                placeholder="Primeiro nome"
                error={errors.first_name}
                {...register('first_name')}
              />
              <TextInput
                label="Sobrenome"
                placeholder="Sobrenome"
                error={errors.last_name}
                {...register('last_name')}
              />
            </SimpleGrid>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Telefone"
                placeholder="Telefone"
                error={errors.phone}
                {...register('phone')}
              />
              <TextInput
                type="email"
                label="E-mail"
                placeholder="E-mail"
                error={errors.email}
                autoComplete="off"
                {...register('email')}
              />
              <TextInput
                type="password"
                label="Senha"
                placeholder="Senha"
                error={errors.password}
                {...register('password')}
              />
            </SimpleGrid>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="CEP"
                placeholder="CEP"
                error={errors.zip}
                {...register('zip')}
              />
              <TextInput
                label="Endereço"
                placeholder="Endereço"
                error={errors.address}
                {...register('address')}
              />
              <TextInput
                label="Número"
                placeholder="Número"
                error={errors.number}
                {...register('number')}
              />
            </SimpleGrid>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Bairro"
                placeholder="Bairro"
                error={errors.district}
                {...register('district')}
              />
              <TextInput
                label="Cidade"
                placeholder="Cidade"
                error={errors.city}
                {...register('city')}
              />
              <Select
                label="Estado"
                placeholder="Estado"
                options={regions}
                error={errors.region}
                {...register('region')}
              />
            </SimpleGrid>
          </VStack>
          <Flex mt="8">
            <Checkbox size="lg" colorScheme="pink" {...register('is_active')}>
              Usuário ativo
            </Checkbox>
          </Flex>
        </FormProvider>
        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button
              onClick={() => {
                if (isModal && onToggle) {
                  onToggle();
                } else {
                  navigate(-1);
                }
              }}
              colorScheme="whiteAlpha"
            >
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Create;
