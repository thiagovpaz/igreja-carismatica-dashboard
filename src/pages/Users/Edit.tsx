import { useCallback, useEffect } from 'react';
import {
  Button,
  Divider,
  Flex,
  Heading,
  HStack,
  SimpleGrid,
  VStack,
} from '@chakra-ui/react';
import { toast } from 'react-toastify';
import { FormProvider, useForm } from 'react-hook-form';
import { NavLink, useNavigate, useParams } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import api from '../../services/api';
import { regions } from '../../utils/regions';

import { TextInput } from '../../components/Form/TextInput';
import { Select } from '../../components/Form/Select';
import { Checkbox } from '../../components/Form/Checkbox';
import { AvatarUpload } from '../../components/Form/AvatarUpload';

interface UserFormData {
  first_name: string;
  last_name: string;
  avatar_file: string;
  email: string;
  password: string;
  phone: string;
  zip: string;
  address: string;
  number: string | number;
  district: string;
  city: string;
  region: string;
  is_active: boolean;
}

const Edit = () => {
  const schema = yup.object({
    first_name: yup.string().required('Campo Obrigatório'),
    last_name: yup.string().required('Campo Obrigatório'),
    email: yup.string().email('E-mail inválido').required('Campo Obrigatório'),
    phone: yup.string().required('Campo Obrigatório'),
  });

  const navigate = useNavigate();
  const { id } = useParams();

  const form = useForm<UserFormData>({
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors, isSubmitting },
  } = form;

  const onSubmit = useCallback(
    async (data: UserFormData) => {
      await toast.promise(
        api.put(`/users/${id}`, data),
        {
          pending: 'Atualizando usuário',
          success: 'Usuário atualizado com sucesso',
          error: 'Falha ao atualizar usuário',
        },
        {
          autoClose: 2000,
          onClose: () => {
            navigate('/users');
          },
        },
      );
    },
    [navigate, id],
  );

  useEffect(() => {
    async function loadUser() {
      const response = await api.get(`/users/${id}`);

      if (response.data) {
        const userData = response.data;

        reset({
          first_name: userData.first_name || '',
          last_name: userData.last_name || '',
          avatar_file: userData.avatar_file || '',
          email: userData.email || '',
          phone: userData.phone || '',
          zip: userData.zip || '',
          address: userData.address || '',
          number: userData.number || '',
          district: userData.district || '',
          city: userData.city || '',
          region: userData.region || '',
          is_active: !!userData.is_active,
        });
      }
    }
    loadUser();
  }, [id, reset]);

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex
        as="form"
        w="100%"
        direction="column"
        justify="space-between"
        align="flex-start"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Heading as="h1" size="lg">
          Editar usuário
        </Heading>
        <Divider my="6" borderColor="gray.700" />
        <FormProvider {...form}>
          <VStack width="100%" spacing="8">
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <AvatarUpload {...register('avatar_file')} />
              <TextInput
                label="Primeiro nome"
                placeholder="Primeiro nome"
                error={errors.first_name}
                {...register('first_name')}
              />
              <TextInput
                label="Sobrenome"
                placeholder="Sobrenome"
                error={errors.last_name}
                {...register('last_name')}
              />
            </SimpleGrid>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Telefone"
                placeholder="Telefone"
                error={errors.phone}
                {...register('phone')}
              />
              <TextInput
                type="email"
                label="E-mail"
                placeholder="E-mail"
                error={errors.email}
                {...register('email')}
              />
              <TextInput
                type="password"
                label="Senha"
                placeholder="Senha"
                error={errors.password}
                {...register('password')}
              />
            </SimpleGrid>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="CEP"
                placeholder="CEP"
                error={errors.zip}
                {...register('zip')}
              />
              <TextInput
                label="Endereço"
                placeholder="Endereço"
                error={errors.address}
                {...register('address')}
              />
              <TextInput
                label="Número"
                placeholder="Número"
                error={errors.number}
                {...register('number')}
              />
            </SimpleGrid>
            <SimpleGrid minChildWidth="240px" spacing={['6', '8']} w="100%">
              <TextInput
                label="Bairro"
                placeholder="Bairro"
                error={errors.district}
                {...register('district')}
              />
              <TextInput
                label="Cidade"
                placeholder="Cidade"
                error={errors.city}
                {...register('city')}
              />
              <Select
                label="Estado"
                placeholder="Estado"
                options={regions}
                error={errors.region}
                {...register('region')}
              />
            </SimpleGrid>
          </VStack>
          <Flex mt="8">
            <Checkbox size="lg" colorScheme="pink" {...register('is_active')}>
              Usuário ativo
            </Checkbox>
          </Flex>
        </FormProvider>
        <Flex mt="8" justify="flex-end">
          <HStack spacing="4">
            <Button as={NavLink} to="/users" colorScheme="whiteAlpha">
              Cancelar
            </Button>
            <Button type="submit" isLoading={isSubmitting} colorScheme="pink">
              Salvar
            </Button>
          </HStack>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Edit;
