/* eslint-disable import/no-duplicates */
import { useState, useEffect, useCallback, useRef } from 'react';
import { toast } from 'react-toastify';
import { useNavigate, NavLink } from 'react-router-dom';
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  Divider,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Icon,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  useDisclosure,
} from '@chakra-ui/react';
import { FaCloudDownloadAlt, FaFilter, FaTrash } from 'react-icons/fa';
import { formatDistanceToNow, parseISO, format } from 'date-fns';
import { ptBR } from 'date-fns/locale';
import {
  defaultInputRanges,
  defaultStaticRanges,
  DateRangePicker,
  Range,
} from 'react-date-range';

import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import * as locales from 'react-date-range/dist/locale';

import fileDownload from 'js-file-download';
import { regions } from '../../utils/regions';
import api from '../../services/api';
import DataTable from '../../components/DataTable';

interface User {
  id: string;
  first_name: string;
  email: string;
  created_at: Date;
}

interface ActionsButtonsProps {
  id: string;
}

interface FilterComponentProps {
  state: any;
  onChangeState(item: Range[]): void;
  region: string;
  onChangeRegion(item: string): void;
}

const FilterComponent: React.FC<FilterComponentProps> = ({
  state,
  onChangeState,
  region,
  onChangeRegion,
}) => {
  const [openReport, setOpenReport] = useState(false);

  const handleGenerateReport = useCallback(async () => {
    try {
      const response = await api.post(
        '/reports/users',
        {
          start_date: state[0].startDate ?? undefined,
          end_date: state[0].endDate ?? undefined,
          region,
        },
        {
          responseType: 'blob',
        },
      );

      fileDownload(response.data, `relatorio-usuarios.pdf`);
    } catch (error) {
      //
    }
  }, [state, region]);

  const handleFilter = () => {
    setOpenReport((s) => !s);
  };

  const removeFilter = () => {
    setOpenReport((s) => !s);
    onChangeRegion('');
    onChangeState([
      { key: 'selection', startDate: undefined, endDate: undefined },
    ]);
  };

  const countFilters = useCallback(() => {
    let f = 0;
    if (region) {
      f += 1;
    }

    if (state[0].endDate !== undefined) {
      f += 1;
    }

    return f;
  }, [region, state]);

  const staticRanges = defaultStaticRanges.map((r) => {
    const new_labels: any = {
      Today: 'Hoje',
      Yesterday: 'Ontem',
      'This Week': 'Esta semana',
      'Last Week': 'Semana passada',
      'This Month': 'Este mês',
      'Last Month': 'Mês passado',
    };

    return { ...r, label: new_labels[r.label!] };
  });

  const inputRanges = defaultInputRanges.map((i) => {
    const new_labels: any = {
      'days up to today': 'Dias antes',
      'days starting today': 'Dias depois',
    };
    return { ...i, label: new_labels[i.label!] };
  });

  return (
    <Flex>
      <Button
        mr={2}
        colorScheme="yellow"
        onClick={() => setOpenReport((s) => !s)}
        leftIcon={<Icon as={FaFilter} />}
      >
        FILTROS ({countFilters()})
      </Button>

      <Modal
        size="full"
        isOpen={openReport}
        onClose={() => setOpenReport((s) => !s)}
      >
        <ModalOverlay />
        <ModalContent bg="gray.800">
          <ModalHeader>RELATÓRIOS</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex flex={1} maxW="850px" direction="column">
              <FormControl py={2} mb={5}>
                <FormLabel>Estado</FormLabel>
                <Select
                  onChange={(e) => {
                    onChangeRegion(e.target.value);
                  }}
                  value={region}
                  placeholder="Estado"
                >
                  {regions.map((r) => (
                    <option key={r.id} value={r.id}>
                      {r.name}
                    </option>
                  ))}
                </Select>
              </FormControl>
              <DateRangePicker
                inputRanges={inputRanges}
                staticRanges={staticRanges}
                onChange={({ selection }) => {
                  onChangeState([selection]);
                }}
                moveRangeOnFirstSelection={false}
                months={2}
                ranges={state}
                direction="horizontal"
                locale={locales.pt}
              />
            </Flex>
          </ModalBody>
          <ModalFooter>
            <Flex w="100%" gap={2} justify="flex-end">
              <Button
                leftIcon={<Icon as={FaFilter} />}
                colorScheme="blue"
                onClick={handleFilter}
              >
                FILTRAR
              </Button>
              <Button
                leftIcon={<Icon as={FaCloudDownloadAlt} />}
                colorScheme="green"
                onClick={handleGenerateReport}
              >
                BAIXAR
              </Button>
              <Button
                leftIcon={<Icon as={FaTrash} />}
                colorScheme="red"
                mr={3}
                onClick={removeFilter}
              >
                FECHAR E LIMPAR
              </Button>
            </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

const Users = () => {
  const modalRef = useRef(null);

  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState(true);
  const [page] = useState(1);
  const [limit] = useState(10);

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [deleteItemId, setDeleteItemId] = useState<string | null>(null);

  const [state, setState] = useState<Range[]>([
    {
      startDate: undefined,
      endDate: undefined,
      key: 'selection',
    },
  ]);

  const [region, setRegion] = useState('');

  useEffect(() => {
    async function loadUsers() {
      try {
        const response = await api.get(
          `/users?region=${region}&start_date=${
            state[0].startDate ? format(state[0].startDate!, 'yyyy-MM-dd') : ''
          }&end_date=${
            state[0].endDate ? format(state[0].endDate!, 'yyyy-MM-dd') : ''
          }`,
        );

        if (response.data) {
          setUsers(response.data);
        }
      } catch (error) {
        //
      } finally {
        setLoading(false);
      }
    }
    loadUsers();
  }, [page, limit, region, state]);

  const handleDelete = useCallback(async () => {
    try {
      toast.promise(api.delete(`/users/${deleteItemId}`), {
        pending: 'Removendo usuário',
        success: 'Usuário removido com sucesso',
        error: 'Erro ao remover usuário',
      });
      setUsers((oldState) => oldState.filter((s) => s.id !== deleteItemId));
    } catch (error) {
      // alert(error);
    }
    onClose();
    setDeleteItemId(null);
  }, [deleteItemId, onClose]);

  const handleDeleteRows = useCallback(async (ids: string[]) => {
    const deleteRows = ids.map(async (id) => {
      await api.delete(`/users/${id}`);
    });
    await Promise.all([deleteRows]);

    toast.success('Usuários removidos com sucesso', { autoClose: 2000 });

    setUsers((oldState) => oldState.filter((item) => !ids.includes(item.id)));
  }, []);

  const ActionsButtons: React.FC<ActionsButtonsProps> = ({ id }) => {
    const navigate = useNavigate();

    const handleEdit = () => {
      navigate(`/users/edit/${id}`);
    };

    return (
      <Flex gap="2">
        <Button
          onClick={handleEdit}
          size="sm"
          colorScheme="blue"
          variant="solid"
        >
          Editar
        </Button>
        <Button
          onClick={() => {
            setDeleteItemId(id);
            onOpen();
          }}
          size="sm"
          colorScheme="red"
          variant="solid"
        >
          Remover
        </Button>
      </Flex>
    );
  };

  return (
    <Flex flex="1" w="100%" minH="100%" bg="gray.800" p={6} direction="column">
      <Flex w="100%" justify="space-between" align="flex-start">
        <Heading as="h1" size="lg">
          Usuários ({users.length})
        </Heading>
        <Button
          as={NavLink}
          to="/users/create"
          size="md"
          fontSize="md"
          colorScheme="pink"
        >
          Novo Usuário
        </Button>
      </Flex>
      <Divider my="6" borderColor="gray.700" />

      <DataTable<User>
        data={users}
        columns={[
          {
            accessorKey: 'first_name',
            cell: (info) => info.getValue(),
            header: () => 'Nome',
            footer: () => 'Nome',
          },
          {
            accessorKey: 'email',
            cell: (info) => info.getValue(),
            header: () => 'E-mail',
            footer: () => 'E-mail',
          },
          {
            accessorKey: 'created_at',
            cell: (info) =>
              formatDistanceToNow(parseISO(info.getValue()), {
                addSuffix: true,
                locale: ptBR,
              }),
            header: () => 'Data de criação',
            footer: () => 'Data de criação',
          },
          {
            accessorKey: 'id',
            cell: (info) => <ActionsButtons id={info.getValue()} />,
            header: () => 'Ações',
            footer: () => 'Ações',
          },
        ]}
        isLoading={loading}
        deleteFn={handleDeleteRows}
        filterComponent={
          <FilterComponent
            state={state}
            onChangeState={(item) => setState(item)}
            region={region}
            onChangeRegion={(item) => setRegion(item)}
          />
        }
      />
      <AlertDialog
        isOpen={isOpen}
        onClose={onClose}
        leastDestructiveRef={modalRef}
      >
        <AlertDialogOverlay>
          <AlertDialogContent bg="gray.800">
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Remover Usuário
            </AlertDialogHeader>

            <AlertDialogBody>
              Você tem certeza que deseja remover ?
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button onClick={onClose} color="gray.900 ">
                Cancelar
              </Button>
              <Button colorScheme="red" onClick={handleDelete} ml={3}>
                Remover
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Flex>
  );
};

export default Users;
