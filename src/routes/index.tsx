import { Routes, Route } from 'react-router-dom';

import PrivateRoute from '../components/PrivateRoute';

import SignIn from '../pages/SignIn';
import Dashboard from '../pages/Dashboard';
import Settings from '../pages/Settings';
import Custom404 from '../pages/Custom404';

import { UserLayout } from '../layouts/UserLayout';
import { AdminLayout } from '../layouts/AdminLayout';

import Users from '../pages/Users';
import UsersCreate from '../pages/Users/Create';
import UsersEdit from '../pages/Users/Edit';
import Notifications from '../pages/Notifications';
import NotificationsCreate from '../pages/Notifications/Create';
import Donations from '../pages/Donations';
import DonationsCreate from '../pages/Donations/Create';
import Testimonies from '../pages/Testimonies';
import TestimoniesCreate from '../pages/Testimonies/Create';
import TestimoniesEdit from '../pages/Testimonies/Edit';
import Content from '../pages/Content';
import ContentEdit from '../pages/Content/Edit';
import UserResetPassword from '../pages/UserAccount/ResetPassword';
import UserAccountConfirm from '../pages/UserAccount/Confirm';
import Privacy from '../pages/Privacy';
import ForgotPassword from '../pages/UserAccount/ForgotPassword';

const AppRoutes = () => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <PrivateRoute>
            <AdminLayout />
          </PrivateRoute>
        }
      >
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/users" element={<Users />} />
        <Route path="/users/create" element={<UsersCreate />} />
        <Route path="/users/edit/:id" element={<UsersEdit />} />
        <Route path="/testimonies" element={<Testimonies />} />
        <Route path="/testimonies/create" element={<TestimoniesCreate />} />
        <Route path="/testimonies/edit/:id" element={<TestimoniesEdit />} />
        <Route path="/donations" element={<Donations />} />
        <Route path="/donations/create" element={<DonationsCreate />} />
        <Route path="/notifications" element={<Notifications />} />
        <Route path="/notifications/create" element={<NotificationsCreate />} />
        <Route path="/pages" element={<Content />} />
        <Route path="/pages/edit/:id" element={<ContentEdit />} />
        <Route path="/settings" element={<Settings />} />
      </Route>
      <Route
        path="/signin"
        element={
          <PrivateRoute checkAuth>
            <SignIn />
          </PrivateRoute>
        }
      />
      <Route path="account" element={<UserLayout />}>
        <Route path="password/reset" element={<UserResetPassword />} />
        <Route path="confirm" element={<UserAccountConfirm />} />
        <Route path="password/forgot" element={<ForgotPassword />} />
      </Route>
      <Route path="/privacy" element={<UserLayout />}>
        <Route path="" element={<Privacy />} />
      </Route>
      <Route path="*" element={<Custom404 />} />
    </Routes>
  );
};

export default AppRoutes;
